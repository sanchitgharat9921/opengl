/*
compilation:
cl.exe /c /EHsc /I C:\freeglut\include OGL.cpp
linking:
 link.exe OGL.obj /LIBPATH:C:\freeglut\lib /SUBSYSTEM:CONSOLE
*/

#include<windows.h>    
#include <stdio.h>
//step1) add header file 
#include "vmath.h" //graphics sathi laganri lib from readbook
#include <gl/glew.h> // add glew.h before GL.h
#include <gl/GL.h>
#include "ogl.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")

using namespace vmath;

HDC ghdc = NULL;
HGLRC ghrc = NULL;

#define WINWIDTH 800
#define WINHEIGHT 600

//Fullscreen
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
HWND ghwnd = NULL;

bool gbActiveWindow = false;

FILE* gpFile = NULL;
int OsWndHight = 0;
int OsWndWidth = 0;

int AppWndHight = 400;
int AppWndWidth = 400;
TCHAR str[255];

//return val | calling convention | funname
// golbal function deeclarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

bool bDone = false;
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

//blobal varibable declarations
//below are the properties of vertex (imaginary point)
//It can have veelocity , gravity, damping, blur.
//below are most common properties of vertex
enum {
	SSG_ATTRIBUTE_POSITION = 0,
	SSG_ATTRIBUTE_COLOR,
	SSG_ATTRIBUTE_TEXCORD,
	SSG_ATTRIBUTE_NORMAL,
};

GLuint vaoTexturePyramid; // Vertex Array Object
GLuint vaoTextureCube;
GLuint vboPositionPyramid; // Vertex Buffer Object
GLuint vboPositionCube;
GLuint vboTexturePyramid;
GLuint vboTextureCube;

GLuint mvpMAtrixUniform; //
GLuint textureSamplerUniform;
GLuint keyPressUniform;
GLfloat anglePyramid;
GLfloat angleCube;

//mat4 -> present in vmath.h
//mat4 ->is a type for 4*4 matrix, ha transformation sathi used hoto
mat4 perspectiveProjectionMatrix; 

//step 1) Texture variables
GLuint smiley_texture;

GLint keyPress = 0;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	void display(void);
	void initialize(void);
	void update(void);

	if (fopen_s(&gpFile, "SSGLog.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Can't open the log file"), TEXT("Error:"), MB_OK);
		return -1;
	}

	//variable declarations
	WNDCLASSEX wndclass; //structure 12 member
	HWND hwnd;           //unsingned int ->>window handle
	MSG msg;             //struct 6 member
	TCHAR szAppName[] = TEXT("MyApp"); // ASCII (americal std code for info exchange) ,UNICODE 

	//code
	//initializaation of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX); //size of class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); //3
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);// 1-> lib fun
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass); //4

	//Get Width and Height
	OsWndHight = GetSystemMetrics(SM_CYSCREEN);
	OsWndWidth = GetSystemMetrics(SM_CXSCREEN);

	//create window 5
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Programmable: 3D Animation with Texture"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();
	ShowWindow(hwnd, iCmdShow);//6
	//UpdateWindow(hwnd);//7

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				display();
				update();
			}

		}

	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str1[] = TEXT("HELLO WORLD !!!");

	//fnction declaration
	void ToggleFullscreen(void);
	void resize(int, int);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:

		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 'f':
		case 'F':
			ToggleFullscreen();
			break;

		case 48:
			fprintf(gpFile, "Pressed 1 \n");
			fflush(gpFile);
			keyPress = 0;
			break;

		case 49:
			fprintf(gpFile, "Pressed 1 \n");
			fflush(gpFile);
			keyPress = 1;
			break;

		case 50:
			keyPress = 2;
			fprintf(gpFile, "Pressed 2 \n");
			fflush(gpFile);
			break;

		case 51:
			keyPress = 3;
			fprintf(gpFile, "Pressed 3 \n");
			fflush(gpFile);
			break;

		case 52:
			keyPress = 4;
			fprintf(gpFile, "Pressed 4 \n");
			fflush(gpFile);
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;

	case WM_CREATE:
		MessageBox(hwnd, TEXT("Welcome To Programmable Pipeline"), TEXT("My_Message"), MB_OK);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam)); //12
}

void initialize(void)
{
	fprintf(gpFile, "[DEBUG] START initialize \n");
	fflush(gpFile);
	void resize(int, int);
	//step2) For texture
	bool loadGlTexture(GLuint*, TCHAR[]);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "[DEBUG] choosePixelFormat failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "[DEBUG] SetPixelFormat failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "[DEBUG] wglCreateContext failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "[DEBUG] wglMakeCurrent failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	//step 3) Enableing the openGl extension by using below line
	// First Programmable programming line
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd); //If extension enable failed for PP , no need to procced further
	}

	fprintf(gpFile, "[DEBUG] After Load texture 1\n");
	fflush(gpFile);
	//OpenGL related logs
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fflush(gpFile);
	fprintf(gpFile, "OpenGL render : %s \n", glGetString(GL_RENDERER));
	fflush(gpFile);
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fflush(gpFile);
	//fprintf(gpFile, "GLSL (Graphics lib shading lang.) : %s \n", glGetString(GL_SHADING_LANG));

	//OpenGL enable extensions
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExt);
	for (int i = 0; i < numExt; i++)
	{
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	// *** VERTEX SHADER ***
	// create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar* vertexShaderSourceCode =

		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCord;" \
		"out vec2 outTexCord;" \
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"\n" \
		"{" \
		"gl_Position = u_mvpMatrix * vPosition;" \
		"outTexCord = vTexCord;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);

	//Nothing to compile so no compile time error checking
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compelation Log: %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar* fragementShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec2 outTexCord;" \
		"uniform sampler2D u_texture_sampler;"
		"uniform int u_keyPress;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
			"if(u_keyPress == 1)" \
			"{" \
				"FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);" \
			"}" \
			"else" \
			"{" \
				"FragColor = texture(u_texture_sampler, outTexCord);" \
			"}" \
		"}";

		//"FragColor = texture(u_texture_sampler, outTexCord);" \
	//u_texture_sampler ha agent ahe jo texel pixel la map karnar ahe
	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragementShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObject);

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	//error checking
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragement Shader Compilation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	//get MVP uniform location

	// ***SHADER PROGRAM **
	//create
	gShaderProgramObject = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//pre-link binding of shader program object with vertex shader position
	//atribute
	glBindAttribLocation(gShaderProgramObject, SSG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, SSG_ATTRIBUTE_TEXCORD, "vTexCord");

	//link shader
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;

	//get link error checking
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	//get mvp uniform location
	mvpMAtrixUniform = glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	textureSamplerUniform = glGetUniformLocation(gShaderProgramObject, "u_texture_sampler");
	keyPressUniform = glGetUniformLocation(gShaderProgramObject, "u_keyPress");


	//step3) For texture , 2 call bez  2 texture image needs to Load(Loading Texture)
	loadGlTexture(&smiley_texture, MAKEINTRESOURCE(SMILEY_BITMAP));


	//*** vertices, color, shader attribs, vbo, vao initialization ***
	
	const GLfloat cubeVertices[] =
	{
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,

	1.0f, 1.0f, -1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,

	-1.0f, 1.0f, -1.0f,
	1.0f, 1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,

	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f,

	1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,

	1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	};


	// ***** For Square ***** //
	glGenVertexArrays(1, &vaoTextureCube);
	glBindVertexArray(vaoTextureCube);

	//For vertex
	glGenBuffers(1, &vboPositionCube);
	glBindBuffer(GL_ARRAY_BUFFER, vboPositionCube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(SSG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//For Texture
	glGenBuffers(1, &vboTextureCube);
	glBindBuffer(GL_ARRAY_BUFFER, vboTextureCube);
	glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(float), NULL, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(SSG_ATTRIBUTE_TEXCORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_TEXCORD);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//For 3D geomertry
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glEnable(GL_CULL_FACE);
	//step4) Enable texture
    //texturebuffer from GPU can access
	//glEnable(GL_TEXTURE_2D);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//set orthographicMatrix to identitu matrix
	perspectiveProjectionMatrix  = mat4::identity();

	fprintf(gpFile, "[DEBUG] END initialize \n");
	fflush(gpFile);
	resize(WINWIDTH, WINHEIGHT);

}
//step5) For texture 
bool loadGlTexture(GLuint* texture, TCHAR resourceID[])
{

	fprintf(gpFile, "[DEBUG] START loadGlTexture()\n");
	fflush(gpFile);
	//varible decalrtions
	bool bResult = false;
	// OS image loading native lines, for every OS this 2 will change
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//code
	//OS Image Loading
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// Internally gluBuild2DMipmaps = glTexImage2D() + glGenerateMipmap() 
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);

	    glGenerateMipmap(GL_TEXTURE_2D);

		//ata data load zalela ahe, handle remove kara. kam zalay
		//OS function
		fflush(gpFile);
	}
	else
	{
		fprintf(gpFile, "[DEBUG] hbitmap false()\n");
		fflush(gpFile);
	}

	return (bResult);
}
void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
					(GLfloat)width / (GLfloat)height,
					0.1f,
					100.0f);
}

void display(void)
{

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//start using OpneGL program object
	glUseProgram(gShaderProgramObject);

	//OpenGL drawing
	// **** For traingle ****//
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	//translate by -10
	mat4 translateMatrix;
	translateMatrix = vmath::translate(0.0f, 0.0f, -5.0f);

	//scale 
	mat4 scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);
	modelViewMatrix = translateMatrix * scaleMatrix;

	//multiplay the modelview and ortho matrix to get modelViewProjection matrix
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpMAtrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//Texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, smiley_texture);
	glUniform1i(textureSamplerUniform, 0);

	GLfloat cubeTexCord[8] = { 0 };
	//bind vao
	glBindVertexArray(vaoTextureCube);
	//give text cord based on keypress
	if (keyPress == 1)
	{
		glUniform1i(keyPressUniform, 0);
		cubeTexCord[0] = 1.0f;
		cubeTexCord[1] = 1.0f;
		cubeTexCord[2] = 0.0f;
		cubeTexCord[3] = 1.0f;
		cubeTexCord[4] = 0.0f;
		cubeTexCord[5] = 0.0f;
		cubeTexCord[6] = 1.0f;
		cubeTexCord[7] = 0.0f;
	}
	else if (keyPress == 2)
	{
		glUniform1i(keyPressUniform, 0);
		cubeTexCord[0] = 0.5f;
		cubeTexCord[1] = 0.5f;
		cubeTexCord[2] = 0.0f;
		cubeTexCord[3] = 0.5f;
		cubeTexCord[4] = 0.0f;
		cubeTexCord[5] = 0.0f;
		cubeTexCord[6] = 0.5f;
		cubeTexCord[7] = 0.0f;
	}
	else if (keyPress == 3)
	{
		glUniform1i(keyPressUniform, 0);
		cubeTexCord[0] = 2.0f;
		cubeTexCord[1] = 2.0f;
		cubeTexCord[2] = 0.0f;
		cubeTexCord[3] = 2.0f;
		cubeTexCord[4] = 0.0f;
		cubeTexCord[5] = 0.0f;
		cubeTexCord[6] = 2.0f;
		cubeTexCord[7] = 0.0f;
	}
	else if (keyPress == 4)
	{
		glUniform1i(keyPressUniform, 0);
		cubeTexCord[0] = 0.5f;
		cubeTexCord[1] = 0.5f;
		cubeTexCord[2] = 0.5f;
		cubeTexCord[3] = 0.5f;
		cubeTexCord[4] = 0.5f;
		cubeTexCord[5] = 0.5f;
		cubeTexCord[6] = 0.5f;
		cubeTexCord[7] = 0.5f;
	}
	else
	{
		glUniform1i(keyPressUniform, 1);
	}

	glBindBuffer(GL_ARRAY_BUFFER, vboTextureCube);
	glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(float), cubeTexCord, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_TEXCORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_TEXCORD);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);


	glBindVertexArray(0);

	//stop using OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);

}

void update(void)
{

	if (anglePyramid >= 360.0f)
	{
		anglePyramid = 0.00f;
	}
	else
	{
		anglePyramid += 0.1f;
	}

	if (angleCube >= 360.0f)
	{
		angleCube = 0.00f;
	}
	else
	{
		angleCube += 0.1f;
	}

}
void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		//set the window placementjust like before the fullscreen
		SetWindowPlacement(ghwnd, &wpPrev);

		//set the window position just like it was before the fullscreen
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// as we are back to normal window show the mouse cursor
		ShowCursor(true);

	}

	//For Texture
	glDeleteTextures(1, &smiley_texture);

	if (vboPositionPyramid)
	{
		glDeleteBuffers(1, &vboPositionPyramid);
		vboPositionPyramid = 0;
	}

	if (vboPositionCube)
	{
		glDeleteBuffers(1, &vboPositionCube);
		vboPositionCube = 0;
	}


	if (vboTexturePyramid)
	{
		glDeleteBuffers(1, &vboTexturePyramid);
		vboTexturePyramid = 0;
	}

	if (vboTextureCube)
	{
		glDeleteBuffers(1, &vboTextureCube);
		vboTextureCube = 0;
	}


	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	//detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	//delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	//delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	//unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "Program is terminated successfully \n");
	fflush(gpFile);
	if (gpFile)
	{
		fclose(gpFile);
		free(gpFile);
	}

	
}

void ToggleFullscreen(void)
{
	//declare monitorinfo type variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//check fullscreen is there or not

	if (gbFullscreen == false)
	{
		//No
			// get cureent window style
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//check the current window style has WS_OVERLAPPEDWINDOW or not
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//if ------------------yes ->removed it
				//to do that
					//get the current window placaement and monitor info

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{

				SetWindowPos(ghwnd, HWND_TOP, 0, 0,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);

				// if both the above things are true then remove WS_OVERLAPPED from style
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				//set the possition of window accordingly so that it will occupy the whole screen
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		//as we are fullscreen hide the cursor conventially
		ShowCursor(false);
		gbFullscreen = true;
		//AS now we have done with fullscreen gbFullscreen = true
	}
	else {
		//yes

			//we are going to convert window to the normal we have to put back WS_OVERLAPPEDWINDOW
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		//set the window placementjust like before the fullscreen
		SetWindowPlacement(ghwnd, &wpPrev);
		
		//set the window position just like it was before the fullscreen
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// as we are back to normal window show the mouse cursor
		ShowCursor(true);
		// gbFullscreen= false
		gbFullscreen = false;
	}
}