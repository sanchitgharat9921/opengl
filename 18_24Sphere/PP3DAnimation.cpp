/*
compilation:
cl.exe /c /EHsc /I C:\freeglut\include OGL.cpp
linking:
 link.exe OGL.obj /LIBPATH:C:\freeglut\lib /SUBSYSTEM:CONSOLE
*/

#include<windows.h>    
#include <stdio.h>
//step1) add header file 
#include "vmath.h" //graphics sathi laganri lib from readbook
#include <gl/glew.h> // add glew.h before GL.h
#include <gl/GL.h>
#include "ogl.h"
#include "Sphere.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib,"Sphere.lib")

using namespace vmath;

HDC ghdc = NULL;
HGLRC ghrc = NULL;

#define WINWIDTH 800
#define WINHEIGHT 600

//Fullscreen
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
HWND ghwnd = NULL;

bool gbActiveWindow = false;

FILE* gpFile = NULL;
int OsWndHight = 0;
int OsWndWidth = 0;

int AppWndHight = 400;
int AppWndWidth = 400;
TCHAR str[255];

//return val | calling convention | funname
// golbal function deeclarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

bool bDone = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

//blobal varibable declarations
//below are the properties of vertex (imaginary point)
//It can have veelocity , gravity, damping, blur.
//below are most common properties of vertex
enum {
	SSG_ATTRIBUTE_POSITION = 0,
	SSG_ATTRIBUTE_COLOR,
	SSG_ATTRIBUTE_TEXCORD,
	SSG_ATTRIBUTE_NORMAL,
};

GLuint vaoSphere;
GLuint vboPositionSphere;
GLuint vboNormalsSphere;
GLuint vboElementsSphere;

//declare 12 uniforms(3 -> matrix, 4 -> lights(with color), 4-> material(white), 1->keyDown)
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint perspectiveProjectionMatrixUniform;

GLuint lKeyPressUniform;

//Light properties
GLuint ldUniform; //light diffuse
GLuint lsUniform; //Light specular
GLuint laUniform; //light Ambient
GLuint lightPositionUniform; //light position

//material properties
GLuint kaUniform; //material ambient
GLuint ksUnifrom; //material specular
GLuint kdUniform; //materail diffuse
GLuint kshiUniform; //mater

struct materialProperties{
	GLfloat materialAmbient[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShyniness;
};
struct materialProperties gMaterialProperties[24] = {1};

bool gbLighting;

//mat4 -> present in vmath.h
//mat4 ->is a type for 4*4 matrix, ha transformation sathi used hoto
mat4 perspectiveProjectionMatrix; 

//For sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gNumVertices;
GLuint gNumElements;

GLuint xAxisRotation;
GLuint yAxisRotation;
GLuint zAxisRotation;
GLfloat angleForXRotation;
GLfloat angleForYRotation;
GLfloat angleForZRotation;

int vWidth = WINWIDTH;
int vHeight = WINHEIGHT;
// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	void display(void);
	void initialize(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass; //structure 12 member
	HWND hwnd;           //unsingned int ->>window handle
	MSG msg;             //struct 6 member
	TCHAR szAppName[] = TEXT("MyApp"); // ASCII (americal std code for info exchange) ,UNICODE 

	//code
	//initializaation of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX); //size of class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION); //2
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); //3
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);// 1-> lib fun
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass); //4

	if (fopen_s(&gpFile, "SSGLog.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Can't open the log file"), TEXT("Error:"), MB_OK);
		return -1;
	}

	//Get Width and Height
	OsWndHight = GetSystemMetrics(SM_CYSCREEN);
	OsWndWidth = GetSystemMetrics(SM_CXSCREEN);


	//create window 5
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Programmable: 3D Animation"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();
	ShowWindow(hwnd, iCmdShow);//6
	//UpdateWindow(hwnd);//7

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				display();
				update();
			}

		}

	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str1[] = TEXT("HELLO WORLD !!!");


	//fnction declaration
	void ToggleFullscreen(void);
	void resize(int, int);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		vWidth = LOWORD(lParam);
		vHeight = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:

		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 'f':
		case 'F':
			ToggleFullscreen();
			break;

		case 'l':
		case 'L':
			if (gbLighting == TRUE)
			{
				gbLighting = FALSE;
			}
			else
			{
				gbLighting = TRUE;
			}
			break;

		case 'x':
		case 'X':
				xAxisRotation = 1;
				yAxisRotation = 0;
				zAxisRotation = 0;
				break;
		case 'y':
		case 'Y':
				xAxisRotation = 0;
				yAxisRotation = 1;
				zAxisRotation = 0;
				break;

		case 'z':
		case 'Z':
				xAxisRotation = 0;
				yAxisRotation = 0;
				zAxisRotation = 1;
				break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;

	case WM_CREATE:
		//MessageBox(hwnd, TEXT("Welcome To Programmable Pipeline"), TEXT("My_Message"), MB_OK);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam)); //12
}

void initialize(void)
{

	fprintf(gpFile, "[DEBUG] Start initialization");
	fflush(gpFile);
	void resize(int, int);
	
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//initialize material properties for 24 spheres
	//For sphere 1 emerald
	
	gMaterialProperties[0].materialAmbient[0] = 0.0215f;
	gMaterialProperties[0].materialAmbient[1] = 1.1745f;
	gMaterialProperties[0].materialAmbient[2] = 0.0215f;
	gMaterialProperties[0].materialAmbient[3] = 1.0f;

	gMaterialProperties[0].materialDiffuse[0] = 0.07568f;
	gMaterialProperties[0].materialDiffuse[1] = 0.61424f;
	gMaterialProperties[0].materialDiffuse[2] = 0.07568f;
	gMaterialProperties[0].materialDiffuse[3] = 1.0f;

	gMaterialProperties[0].materialSpecular[0] = 0.633f;
	gMaterialProperties[0].materialSpecular[1] = 0.727811f;
	gMaterialProperties[0].materialSpecular[2] = 0.633f;
	gMaterialProperties[0].materialSpecular[3] = 1.0f;

	gMaterialProperties[0].materialShyniness = 0.6f * 128;

	// sphere 2 jade
	gMaterialProperties[1].materialAmbient[0] = 0.135f;
	gMaterialProperties[1].materialAmbient[1] = 0.22525f;
	gMaterialProperties[1].materialAmbient[2] = 0.1575f;
	gMaterialProperties[1].materialAmbient[3] = 1.0f;

	gMaterialProperties[1].materialDiffuse[0] = 0.54f;
	gMaterialProperties[1].materialDiffuse[1] = 0.89f;
	gMaterialProperties[1].materialDiffuse[2] = 0.63f;
	gMaterialProperties[1].materialDiffuse[3] = 1.0f;
	

	gMaterialProperties[1].materialSpecular[0] = 0.316228f;
	gMaterialProperties[1].materialSpecular[1] = 0.316228f;
	gMaterialProperties[1].materialSpecular[2] = 0.316228f;
	gMaterialProperties[1].materialSpecular[3] = 1.0f;

	gMaterialProperties[1].materialShyniness = 0.1f * 128;

	//sphere 3 obsidian
	gMaterialProperties[2].materialAmbient[0] = 0.05375;
	gMaterialProperties[2].materialAmbient[1] = 0.05;
	gMaterialProperties[2].materialAmbient[2] = 0.06625;

	gMaterialProperties[2].materialDiffuse[0] = 0.18275;
	gMaterialProperties[2].materialDiffuse[1] = 0.17;
	gMaterialProperties[2].materialDiffuse[2] = 0.22525;

	gMaterialProperties[2].materialSpecular[0] = 0.332741;
	gMaterialProperties[2].materialSpecular[1] = 0.328634;
	gMaterialProperties[2].materialSpecular[2] = 0.346435;

	gMaterialProperties[2].materialShyniness = 0.3 * 128;

	//sphere 4 pearl
	
	gMaterialProperties[3].materialAmbient[0] = 0.25;
	gMaterialProperties[3].materialAmbient[1] = 0.20725;
	gMaterialProperties[3].materialAmbient[2] = 0.20725;

	gMaterialProperties[3].materialDiffuse[0] = 1.0;
	gMaterialProperties[3].materialDiffuse[1] = 0.829;
	gMaterialProperties[3].materialDiffuse[2] = 0.829;

	gMaterialProperties[3].materialSpecular[0] = 0.296648;
	gMaterialProperties[3].materialSpecular[1] = 0.296648;
	gMaterialProperties[3].materialSpecular[2] = 0.296648;

	gMaterialProperties[3].materialShyniness = 0.088 * 128;

	//sphere 5 ruby

	gMaterialProperties[4].materialAmbient[0] = 0.1745;
	gMaterialProperties[4].materialAmbient[1] = 0.01175;
	gMaterialProperties[4].materialAmbient[2] = 0.01175;

	gMaterialProperties[4].materialDiffuse[0] = 0.61424;
	gMaterialProperties[4].materialDiffuse[1] = 0.04136;
	gMaterialProperties[4].materialDiffuse[2] = 0.04136;

	gMaterialProperties[4].materialSpecular[0] = 0.727811;
	gMaterialProperties[4].materialSpecular[1] = 0.626959;
	gMaterialProperties[4].materialSpecular[2] = 0.626959;

	gMaterialProperties[4].materialShyniness = 0.6 * 128;

	//sphere 6 turquoise

	gMaterialProperties[5].materialAmbient[0] = 0.1;
	gMaterialProperties[5].materialAmbient[1] = 0.18725;
	gMaterialProperties[5].materialAmbient[2] = 0.1745;

	gMaterialProperties[5].materialDiffuse[0] = 0.396;
	gMaterialProperties[5].materialDiffuse[1] = 0.74151;
	gMaterialProperties[5].materialDiffuse[2] = 0.69102;

	gMaterialProperties[5].materialSpecular[0] = 0.297254;
	gMaterialProperties[5].materialSpecular[1] = 0.30829;
	gMaterialProperties[5].materialSpecular[2] = 0.306678;

	gMaterialProperties[5].materialShyniness = 0.1 * 128;

	//1st sphere 2nd colm brass

	gMaterialProperties[6].materialAmbient[0] = 0.329412;
	gMaterialProperties[6].materialAmbient[1] = 0.223529;
	gMaterialProperties[6].materialAmbient[2] = 0.027451;

	gMaterialProperties[6].materialDiffuse[0] = 0.780392;
	gMaterialProperties[6].materialDiffuse[1] = 0.5686627;
	gMaterialProperties[6].materialDiffuse[2] = 0.113725;

	gMaterialProperties[6].materialSpecular[0] = 0.992157;
	gMaterialProperties[6].materialSpecular[1] = 0.941175;
	gMaterialProperties[6].materialSpecular[2] = 0.807843;

	gMaterialProperties[6].materialShyniness = 0.21794872 * 128;


	//2nd sphere 2nd colm brass

	gMaterialProperties[7].materialAmbient[0] = 0.2125;
	gMaterialProperties[7].materialAmbient[1] = 0.12754;
	gMaterialProperties[7].materialAmbient[2] = 0.054;

	gMaterialProperties[7].materialDiffuse[0] = 0.714;
	gMaterialProperties[7].materialDiffuse[1] = 0.4284;
	gMaterialProperties[7].materialDiffuse[2] = 0.18144;

	gMaterialProperties[7].materialSpecular[0] = 0.393548;
	gMaterialProperties[7].materialSpecular[1] = 0.271906;
	gMaterialProperties[7].materialSpecular[2] = 0.166721;

	gMaterialProperties[7].materialShyniness = 0.2 * 128;

	//3rd sphere 2nd colm chrome

	gMaterialProperties[8].materialAmbient[0] = 0.25;
	gMaterialProperties[8].materialAmbient[1] = 0.25;
	gMaterialProperties[8].materialAmbient[2] = 0.25;

	gMaterialProperties[8].materialDiffuse[0] = 0.4;
	gMaterialProperties[8].materialDiffuse[1] = 0.4;
	gMaterialProperties[8].materialDiffuse[2] = 0.4;

	gMaterialProperties[8].materialSpecular[0] = 0.774597;
	gMaterialProperties[8].materialSpecular[1] = 0.774597;
	gMaterialProperties[8].materialSpecular[2] = 0.774597;

	gMaterialProperties[8].materialShyniness = 0.6 * 128;


	//4th sphere 2nd colm copper

	gMaterialProperties[9].materialAmbient[0] = 0.19125;
	gMaterialProperties[9].materialAmbient[1] = 0.0735;
	gMaterialProperties[9].materialAmbient[2] = 0.0225;

	gMaterialProperties[9].materialDiffuse[0] = 0.7038;
	gMaterialProperties[9].materialDiffuse[1] = 0.27048;
	gMaterialProperties[9].materialDiffuse[2] = 0.0828;

	gMaterialProperties[9].materialSpecular[0] = 0.256777;
	gMaterialProperties[9].materialSpecular[1] = 0.137622;
	gMaterialProperties[9].materialSpecular[2] = 0.086014;

	gMaterialProperties[9].materialShyniness = 0.1 * 128;


	//5th sphere 2nd colm gold

	gMaterialProperties[10].materialAmbient[0] = 0.24725;
	gMaterialProperties[10].materialAmbient[1] = 0.1995;
	gMaterialProperties[10].materialAmbient[2] = 0.0745;

	gMaterialProperties[10].materialDiffuse[0] = 0.75164;
	gMaterialProperties[10].materialDiffuse[1] = 0.60648;
	gMaterialProperties[10].materialDiffuse[2] = 0.22648;

	gMaterialProperties[10].materialSpecular[0] = 0.628281;
	gMaterialProperties[10].materialSpecular[1] = 0.555802;
	gMaterialProperties[10].materialSpecular[2] = 0.366065;

	gMaterialProperties[10].materialShyniness = 0.4 * 128;

	//6th sphere 2nd colm silver
	gMaterialProperties[11].materialAmbient[0] = 0.19225;
	gMaterialProperties[11].materialAmbient[1] = 0.19225;
	gMaterialProperties[11].materialAmbient[2] = 0.19225;

	gMaterialProperties[11].materialDiffuse[0] = 0.50754;
	gMaterialProperties[11].materialDiffuse[1] = 0.50754;
	gMaterialProperties[11].materialDiffuse[2] = 0.50754;

	gMaterialProperties[11].materialSpecular[0] = 0.508273;
	gMaterialProperties[11].materialSpecular[1] = 0.508273;
	gMaterialProperties[11].materialSpecular[2] = 0.808273;

	gMaterialProperties[11].materialShyniness = 0.4 * 128;


	//1st sphere 3rd colm black
	gMaterialProperties[12].materialAmbient[0] = 0.0;
	gMaterialProperties[12].materialAmbient[1] = 0.0;
	gMaterialProperties[12].materialAmbient[2] = 0.0;

	gMaterialProperties[12].materialDiffuse[0] = 0.01;
	gMaterialProperties[12].materialDiffuse[1] = 0.01;
	gMaterialProperties[12].materialDiffuse[2] = 0.01;

	gMaterialProperties[12].materialSpecular[0] = 0.50;
	gMaterialProperties[12].materialSpecular[1] = 0.50;
	gMaterialProperties[12].materialSpecular[2] = 0.50;

	gMaterialProperties[12].materialShyniness = 0.25 * 128;


	//2nd sphere 2nd colm cyan
	gMaterialProperties[13].materialAmbient[0] = 0.0;
	gMaterialProperties[13].materialAmbient[1] = 0.1;
	gMaterialProperties[13].materialAmbient[2] = 0.06;

	gMaterialProperties[13].materialDiffuse[0] = 0.0;
	gMaterialProperties[13].materialDiffuse[1] = 0.50980392;
	gMaterialProperties[13].materialDiffuse[2] = 0.50980392;

	gMaterialProperties[13].materialSpecular[0] = 0.50196078;
	gMaterialProperties[13].materialSpecular[1] = 0.50196078;
	gMaterialProperties[13].materialSpecular[2] = 0.50196078;

	gMaterialProperties[13].materialShyniness = 0.25 * 128;


	//3rd sphere 2nd colm green
	gMaterialProperties[14].materialAmbient[0] = 0.0;
	gMaterialProperties[14].materialAmbient[1] = 0.0;
	gMaterialProperties[14].materialAmbient[2] = 0.0;

	gMaterialProperties[14].materialDiffuse[0] = 0.1;
	gMaterialProperties[14].materialDiffuse[1] = 0.35;
	gMaterialProperties[14].materialDiffuse[2] = 0.1;

	gMaterialProperties[14].materialSpecular[0] = 0.45;
	gMaterialProperties[14].materialSpecular[1] = 0.45;
	gMaterialProperties[14].materialSpecular[2] = 0.45;

	gMaterialProperties[14].materialShyniness = 0.25 * 128;

	//4rd sphere 3nd colm red
	gMaterialProperties[15].materialAmbient[0] = 0.0;
	gMaterialProperties[15].materialAmbient[1] = 0.0;
	gMaterialProperties[15].materialAmbient[2] = 0.0;

	gMaterialProperties[15].materialDiffuse[0] = 0.5;
	gMaterialProperties[15].materialDiffuse[1] = 0.0;
	gMaterialProperties[15].materialDiffuse[2] = 0.0;

	gMaterialProperties[15].materialSpecular[0] = 0.7;
	gMaterialProperties[15].materialSpecular[1] = 0.6;
	gMaterialProperties[15].materialSpecular[2] = 0.6;

	gMaterialProperties[15].materialShyniness = 0.25 * 128;


	//5th sphere 3nd colm white
	gMaterialProperties[16].materialAmbient[0] = 0.0;
	gMaterialProperties[16].materialAmbient[1] = 0.0;
	gMaterialProperties[16].materialAmbient[2] = 0.0;

	gMaterialProperties[16].materialDiffuse[0] = 0.55;
	gMaterialProperties[16].materialDiffuse[1] = 0.55;
	gMaterialProperties[16].materialDiffuse[2] = 0.55;

	gMaterialProperties[16].materialSpecular[0] = 0.70;
	gMaterialProperties[16].materialSpecular[1] = 0.70;
	gMaterialProperties[16].materialSpecular[2] = 0.70;

	gMaterialProperties[16].materialShyniness = 0.25 * 128;

	//6th sphere 3nd colm yellow plastic
	gMaterialProperties[17].materialAmbient[0] = 0.0;
	gMaterialProperties[17].materialAmbient[1] = 0.0;
	gMaterialProperties[17].materialAmbient[2] = 0.0;

	gMaterialProperties[17].materialDiffuse[0] = 0.5;
	gMaterialProperties[17].materialDiffuse[1] = 0.5;
	gMaterialProperties[17].materialDiffuse[2] = 0.0;

	gMaterialProperties[17].materialSpecular[0] = 0.60;
	gMaterialProperties[17].materialSpecular[1] = 0.60;
	gMaterialProperties[17].materialSpecular[2] = 0.50;

	gMaterialProperties[17].materialShyniness = 0.25 * 128;

	//1st sphere 4th colm black
	gMaterialProperties[18].materialAmbient[0] = 0.02;
	gMaterialProperties[18].materialAmbient[1] = 0.02;
	gMaterialProperties[18].materialAmbient[2] = 0.02;

	gMaterialProperties[18].materialDiffuse[0] = 0.01;
	gMaterialProperties[18].materialDiffuse[1] = 0.01;
	gMaterialProperties[18].materialDiffuse[2] = 0.01;

	gMaterialProperties[18].materialSpecular[0] = 0.4;
	gMaterialProperties[18].materialSpecular[1] = 0.4;
	gMaterialProperties[18].materialSpecular[2] = 0.4;

	gMaterialProperties[18].materialShyniness = 0.078125 * 128;

	
	//2nd sphere 4th colm cyan
	gMaterialProperties[19].materialAmbient[0] = 0.0;
	gMaterialProperties[19].materialAmbient[1] = 0.05;
	gMaterialProperties[19].materialAmbient[2] = 0.05;

	gMaterialProperties[19].materialDiffuse[0] = 0.4;
	gMaterialProperties[19].materialDiffuse[1] = 0.5;
	gMaterialProperties[19].materialDiffuse[2] = 0.5;

	gMaterialProperties[19].materialSpecular[0] = 0.04;
	gMaterialProperties[19].materialSpecular[1] = 0.7;
	gMaterialProperties[19].materialSpecular[2] = 0.7;

	gMaterialProperties[19].materialShyniness = 0.078125 * 128;

		//3rd sphere 4th colm green
	gMaterialProperties[20].materialAmbient[0] = 0.0;
	gMaterialProperties[20].materialAmbient[1] = 0.05;
	gMaterialProperties[20].materialAmbient[2] = 0.0;

	gMaterialProperties[20].materialDiffuse[0] = 0.4;
	gMaterialProperties[20].materialDiffuse[1] = 0.5;
	gMaterialProperties[20].materialDiffuse[2] = 0.4;

	gMaterialProperties[20].materialSpecular[0] = 0.04;
	gMaterialProperties[20].materialSpecular[1] = 0.7;
	gMaterialProperties[20].materialSpecular[2] = 0.04;

	gMaterialProperties[20].materialShyniness = 0.078125 * 128;


	//4th sphere 4th colm red
	gMaterialProperties[21].materialAmbient[0] = 0.05;
	gMaterialProperties[21].materialAmbient[1] = 0.0;
	gMaterialProperties[21].materialAmbient[2] = 0.0;

	gMaterialProperties[21].materialDiffuse[0] = 0.5;
	gMaterialProperties[21].materialDiffuse[1] = 0.4;
	gMaterialProperties[21].materialDiffuse[2] = 0.4;

	gMaterialProperties[21].materialSpecular[0] = 0.7;
	gMaterialProperties[21].materialSpecular[1] = 0.04;
	gMaterialProperties[21].materialSpecular[2] = 0.04;

	gMaterialProperties[21].materialShyniness = 0.078125 * 128;

	//5th sphere 4th colm white
	gMaterialProperties[22].materialAmbient[0] = 0.05;
	gMaterialProperties[22].materialAmbient[1] = 0.05;
	gMaterialProperties[22].materialAmbient[2] = 0.05;

	gMaterialProperties[22].materialDiffuse[0] = 0.5;
	gMaterialProperties[22].materialDiffuse[1] = 0.5;
	gMaterialProperties[22].materialDiffuse[2] = 0.5;

	gMaterialProperties[22].materialSpecular[0] = 0.7;
	gMaterialProperties[22].materialSpecular[1] = 0.7;
	gMaterialProperties[22].materialSpecular[2] = 0.7;

	gMaterialProperties[22].materialShyniness = 0.078125 * 128;


	//6th sphere 4th colm yellow rubber

	gMaterialProperties[23].materialAmbient[0] = 0.05;
	gMaterialProperties[23].materialAmbient[1] = 0.05;
	gMaterialProperties[23].materialAmbient[2] = 0.0;

	gMaterialProperties[23].materialDiffuse[0] = 0.5;
	gMaterialProperties[23].materialDiffuse[1] = 0.5;
	gMaterialProperties[23].materialDiffuse[2] = 0.4;

	gMaterialProperties[23].materialSpecular[0] = 0.7;
	gMaterialProperties[23].materialSpecular[1] = 0.7;
	gMaterialProperties[23].materialSpecular[2] = 0.04;

	gMaterialProperties[23].materialShyniness = 0.078125 * 128;

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "choosePixelFormat failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, " SetPixelFormat failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	//step 3) Enableing the openGl extension by using below line
	// First Programmable programming line
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd); //If extension enable failed for PP , no need to procced further
	}

	//OpenGL related logs
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fflush(gpFile);

	fprintf(gpFile, "OpenGL render : %s \n", glGetString(GL_RENDERER));
	fflush(gpFile);

	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fflush(gpFile);

	//fprintf(gpFile, "GLSL (Graphics lib shading lang.) : %s \n", glGetString(GL_SHADING_LANG));

	//OpenGL enable extensions
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExt);
	for (int i = 0; i < numExt; i++)
	{
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
		fflush(gpFile);
	}

	// *** VERTEX SHADER ***
	// create shader
	fprintf(gpFile, "[DEBUG] start vertex shader \n");
	fflush(gpFile);
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		//provide source code to shader
	const GLchar* vertexShaderSourceCode =
		/* core
		-> Apan openGL la sangato core profile vapar
		means programmable pipeline vapar.
		ithe dusare options pan ahet
		eg; compatibility core-> for fixed function pipeline vaparayachi ahe
		hybrid core -> FFP + programable pipeline vaparayachi ahe
		ha word 3.2 nantar important zala, 4.1 nanatr lok core word used karayala lagae
		core tells shader compiler to check wheather deprecated functions are not used.

		in vec4 vPosition
		-->
		jeva jeva shader madhe in/out used kele jatat te shader chaya GLSL chya compiler che
		specifier ahet
		in :data ya varibale madhun shader madhe yenar ahe from main program
		vec4
		vPostion : he vertex chi property ahe asa data ahe ,
		ani vec4 ha tycha type ahe te represent karnyasathi
		vec4 : 4 components (x,v,z,w) asalela data

		uniform mat4 u_mvpMatrix
		-->
		uniform : uniform navacha data satat badlnara data shader made yenar ahe.
		in madhun yenara data akdacha yeto ani uniform madhala data punha
		punha yeto.

		in: yat traingle data devala ahe bez yache cordinates chage nahi honar
		eg. traingle cha square nahi honar
		uniform : contsians matrix bez yacha data satat chage honar ahe

		*/
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"\n" \
		"in vec3 vNormal;" \
		"\n" \
		"uniform mat4 u_modelMatrix;" \
		"\n" \
		"uniform mat4 u_viewMatrix;" \
		"\n" \
		"uniform mat4 u_projectionMatrix;" \
		"\n" \
		"uniform int u_lKeyPressed;" \
		"\n" \
		"uniform vec4 u_lightPosition;" \
		"\n" \
		"out vec3 transformNormal;" \
		"\n" \
		"out vec3 lightDirection;" \
		"\n" \
		"out vec3 viewVector;" \
		"\n" \
		"void main(void)" \
		"\n" \
		"{" \
		"\n" \
		"if(u_lKeyPressed == 1)" \
		"\n" \
			"{" \
				"\n" \
				// step1: cal eye cordiantes
				"vec4 eyeCordinate = u_viewMatrix * u_modelMatrix * vPosition;" \
				"\n" \
				// step2: cal transform normals
				"transformNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal;" \
				"\n" \
				// step3: cal source of light
				"lightDirection = vec3(u_lightPosition - eyeCordinate);" \
				"\n" \
				// step5: cal view vector
				"viewVector = -eyeCordinate.xyz;" \
				"\n" \
			"}" \

		"gl_Position = u_projectionMatrix * u_modelMatrix * u_viewMatrix * vPosition;" \
		
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);
	
	fprintf(gpFile, "[DEBUG] start complie vertex shader \n");
	fflush(gpFile);

	//Nothing to compile so no compile time error checking
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compelation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	fprintf(gpFile, "[DEBUG] end complie vertex shader \n");
	fflush(gpFile);
	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar* fragementShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"vec4 color;" \
		"\n" \
		"in vec3 transformNormal;" \
		"\n" \
		"in vec3 lightDirection;" \
		"\n" \
		"in vec3 viewVector;" \
		"\n" \
		"uniform int u_lKeyPressed;" \
		"\n" \
		"uniform vec3 u_la;" \
		"\n" \
		"uniform vec3 u_ld;" \
		"\n" \
		"uniform vec3 u_ls;" \
		"\n" \
		"uniform vec3 u_ka;" \
		"\n" \
		"uniform vec3 u_kd;" \
		"\n" \
		"uniform vec3 u_ks;" \
		"\n" \
		"uniform float u_kshi;" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyPressed == 1)" \
		"{" \
			"\n" \
			"vec3 normalizeTransformNormal = normalize(transformNormal);" \
			"\n" \
			"vec3 normalizeLightDirection = normalize(lightDirection);" \
			"\n" \
			"vec3 normalizeViewVector = normalize(viewVector);" \
			"\n" \
			// cal reflection vector
			"vec3 reflectionVector = reflect(-normalizeLightDirection,normalizeTransformNormal);" \
			"\n" \
			//cal diffuse light by using light eq.
			// Ia =la * ka
			"vec3 lightAmbient = u_la * u_ka;" \
			"\n" \
			// Id = ld * kd * (L.N)
			"vec3 lightDiffuse = u_ld * u_kd * max(dot(normalizeLightDirection, normalizeTransformNormal), 0.0f);" \
			"\n" \
			// Is = ls * ks * (R.V)^shi
			"vec3 lightSpecular = u_ls * u_ks * pow(max(dot(reflectionVector, normalizeViewVector), 0.0f),u_kshi );" \
			"\n" \
			// total light intensity = Ia + Id + Is
			"vec3 fongAdsLight = lightAmbient + lightDiffuse + lightSpecular;" \
			"\n" \
			"color = vec4(fongAdsLight, 1.0f);" \
		"}" \
		"else" \
		"{" \
			"color = vec4(1.0f, 1.0f, 1.0f, 1.0f);" \
		"}" \
		
		"FragColor = color;" \

		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragementShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObject);
	
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	//error checking
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragement Shader Compelation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	//get MVP uniform location
		
	// ***SHADER PROGRAM **
	//create
	gShaderProgramObject = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//pre-link binding of shader program object with vertex shader position
	//atribute
	glBindAttribLocation(gShaderProgramObject, SSG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, SSG_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObject, SSG_ATTRIBUTE_NORMAL, "vNormal");

	//link shader
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;

	//get link error checking
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//------------------ Get mvp uniform location  -------------------------------//
	modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	perspectiveProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	lKeyPressUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");

	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosition");

	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUnifrom = glGetUniformLocation(gShaderProgramObject, "u_ks");
	kshiUniform = glGetUniformLocation(gShaderProgramObject, "u_kshi");
	//-------------------------------------------------------------------------------//

	//*** vertices, color, shader attribs, vbo, vao initialization ***

	//Get sphere vertices and normal
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();
	fprintf(gpFile, "Sphere [gNumVertices gNumElements ]: [%u %u]\n", gNumVertices, 
		gNumElements);
	

	// ***** For Square ***** //
	glGenVertexArrays(1, &vaoSphere);
	glBindVertexArray(vaoSphere);

	//For vertex
	glGenBuffers(1, &vboPositionSphere);
	glBindBuffer(GL_ARRAY_BUFFER, vboPositionSphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//For normals
	glGenBuffers(1, &vboNormalsSphere);
	glBindBuffer(GL_ARRAY_BUFFER, vboNormalsSphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vbo element
	glGenBuffers(1, &vboElementsSphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboElementsSphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//For 3D geomertry
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	gbLighting = false;

	//set orthographicMatrix to identitu matrix
	perspectiveProjectionMatrix  = mat4::identity();

	fprintf(gpFile, "[DEBUG] End initialization \n");
	fflush(gpFile);
	resize(WINWIDTH, WINHEIGHT);

}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
					(GLfloat)width / (GLfloat)height,
					0.1f,
					100.0f);
}

void display(void)
{

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//start using OpneGL program object
	glUseProgram(gShaderProgramObject);

	//Light
	if (gbLighting == true)
	{
		//Enable ligthing we have 4 uniform (1 -> keypressed, 3 -> lights)
		glUniform1i(lKeyPressUniform, 1);
		glUniform3f(laUniform, 0.0f, 0.0f, 0.0f);
		glUniform3f(lsUniform, 1.0f, 1.0f, 1.0f);
		glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f); //light color
		GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f }; // w= 1 -> positional light , 0-> directional light
		glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);

		#if 1
		//for light mov
		if(xAxisRotation)
		{
		angleForXRotation +=0.005f;
		lightPosition[0] = 0.0f;
		lightPosition[1] = (GLfloat)cos(angleForXRotation) * 5;
		lightPosition[2] = (GLfloat)sin(angleForXRotation) * 5;
		lightPosition[3] = 1.0f;
		glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);
	
		}
		else if(yAxisRotation)
		{
		angleForYRotation +=0.005f;
		lightPosition[0] = (GLfloat)cos(angleForYRotation) * 5;
		lightPosition[1] = 0.0f;
		lightPosition[2] = (GLfloat)sin(angleForYRotation) * 5;
		lightPosition[3] = 1.0f;
		glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);
		}
		else if(zAxisRotation)
		{	
		angleForZRotation +=0.005f;
		lightPosition[0] = (GLfloat)cos(angleForZRotation) * 5;
		lightPosition[1] = (GLfloat)sin(angleForZRotation) * 5;
		lightPosition[2] = 0.0f;
		lightPosition[3] = 1.0f;
		glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);
		}
		
		#endif
	}
	else
	{
		glUniform1i(lKeyPressUniform, 0);
	}

	//OpenGL drawing
	// **** For traingle ****//
	//sphere 1
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 projectionMatrix = mat4::identity();
	mat4 translateMatrix = mat4::identity();;

	int k = 0;
	for(int i = 0; i < 6; i++)
	{
		for(int j=0; j< 4; j++)
		{

			translateMatrix = vmath::translate(0.0f, 0.0f, -1.50f);
			modelMatrix = translateMatrix; 
			projectionMatrix = perspectiveProjectionMatrix ;
			glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);//model space to world space 
			glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix); //for world to view space
			glUniformMatrix4fv(perspectiveProjectionMatrixUniform, 1, GL_FALSE, projectionMatrix); // for view to clip space

			#if 0
			glUniform3f(kaUniform, 0.05375f, 0.05f, 0.06625f);
			glUniform3f(ksUnifrom, 0.3327f, 0.3286f, 0.346f);
			glUniform3f(kdUniform, 0.18275f, 0.17f, 0.22525f); // material diffusee
			glUniform1f(kshiUniform, 0.3 * 128);
			#endif

			#if 1
			glUniform3fv(kaUniform, 1, (GLfloat*)gMaterialProperties[k].materialAmbient);
			glUniform3fv(ksUnifrom, 1, (GLfloat*)gMaterialProperties[k].materialSpecular);
			glUniform3fv(kdUniform, 1, (GLfloat*)gMaterialProperties[k].materialDiffuse); // material diffusee
			glUniform1f(kshiUniform, (GLfloat)gMaterialProperties[k].materialShyniness);
			#endif


			//bind vao
			glViewport( j * vWidth/4 , i * vHeight/6 , (GLsizei)vWidth/6, (GLsizei)vHeight/6);
			glBindVertexArray(vaoSphere);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboElementsSphere);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);

			modelMatrix = mat4::identity();
			viewMatrix = mat4::identity();
			projectionMatrix = mat4::identity();
			translateMatrix = mat4::identity();
			k++;
		}
		
	}
    
	glUseProgram(0);

	SwapBuffers(ghdc);

}

void update(void)
{


}
void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		//set the window placementjust like before the fullscreen
		SetWindowPlacement(ghwnd, &wpPrev);

		//set the window position just like it was before the fullscreen
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// as we are back to normal window show the mouse cursor
		ShowCursor(true);

	}

	if (vboPositionSphere)
	{
		glDeleteBuffers(1, &vboPositionSphere);
		vboPositionSphere = 0;
	}


	if (vboNormalsSphere)
	{
		glDeleteBuffers(1, &vboNormalsSphere);
		vboNormalsSphere = 0;
	}


	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	//detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	//delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	//delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	//unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "Program is terminated successfully \n");
	fflush(gpFile);
	if (gpFile)
	{
		fclose(gpFile);
		free(gpFile);
	}

	
}

void ToggleFullscreen(void)
{
	//declare monitorinfo type variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//check fullscreen is there or not

	if (gbFullscreen == false)
	{
		//No
			// get cureent window style
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//check the current window style has WS_OVERLAPPEDWINDOW or not
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//if ------------------yes ->removed it
				//to do that
					//get the current window placaement and monitor info

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{

				SetWindowPos(ghwnd, HWND_TOP, 0, 0,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);

				// if both the above things are true then remove WS_OVERLAPPED from style
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				//set the possition of window accordingly so that it will occupy the whole screen
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		//as we are fullscreen hide the cursor conventially
		ShowCursor(false);
		gbFullscreen = true;
		//AS now we have done with fullscreen gbFullscreen = true
	}
	else {
		//yes

			//we are going to convert window to the normal we have to put back WS_OVERLAPPEDWINDOW
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		//set the window placementjust like before the fullscreen
		SetWindowPlacement(ghwnd, &wpPrev);
		
		//set the window position just like it was before the fullscreen
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// as we are back to normal window show the mouse cursor
		ShowCursor(true);
		// gbFullscreen= false
		gbFullscreen = false;
	}
}