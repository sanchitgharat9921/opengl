/*
compilation:
cl.exe /c /EHsc /I C:\freeglut\include OGL.cpp
linking:
 link.exe OGL.obj /LIBPATH:C:\freeglut\lib /SUBSYSTEM:CONSOLE
*/

#include<windows.h>    
#include <stdio.h>
//step1) add header file 
#include "vmath.h" //graphics sathi laganri lib from readbook
#include <gl/glew.h> // add glew.h before GL.h
#include <gl/GL.h>
#include "ogl.h"
#include "Sphere.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib,"Sphere.lib")

using namespace vmath;

HDC ghdc = NULL;
HGLRC ghrc = NULL;

#define WINWIDTH 800
#define WINHEIGHT 600

//Fullscreen
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
HWND ghwnd = NULL;

bool gbActiveWindow = false;

FILE* gpFile = NULL;
int OsWndHight = 0;
int OsWndWidth = 0;

int AppWndHight = 400;
int AppWndWidth = 400;
TCHAR str[255];

//return val | calling convention | funname
// golbal function deeclarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

bool bDone = false;
//For per-vertex
GLuint gVertexShaderObjectForPerVertex;
GLuint gFragmentShaderObjectForPerVertex;
GLuint gShaderProgramObjectForPerVertex;

//For per-fragment
GLuint gVertexShaderObjectForPerFragment;
GLuint gFragmentShaderObjectForPerFragment;
GLuint gShaderProgramObjectForPerFragment;

//blobal varibable declarations
//below are the properties of vertex (imaginary point)
//It can have veelocity , gravity, damping, blur.
//below are most common properties of vertex
enum {
	SSG_ATTRIBUTE_POSITION = 0,
	SSG_ATTRIBUTE_COLOR,
	SSG_ATTRIBUTE_TEXCORD,
	SSG_ATTRIBUTE_NORMAL,
};

GLuint vaoSphere;
GLuint vboPositionSphere;
GLuint vboNormalsSphere;
GLuint vboElementsSphere;

//declare 12 uniforms(3 -> matrix, 4 -> lights(with color), 4-> material(white), 1->keyDown)
GLuint modelMatrixUniformForPerVertex;
GLuint viewMatrixUniformForPerVertex;
GLuint perspectiveProjectionMatrixUniformForPerVertex;

GLuint lKeyPressUniformForPerVertex;
//Light properties
GLuint ldUniformForPerVertex; //light diffuse
GLuint lsUniformForPerVertex; //Light specular
GLuint laUniformForPerVertex; //light Ambient
GLuint lightPositionUniformForPerVertex; //light position
//material properties
GLuint kaUniformForPerVertex; //material ambient
GLuint ksUnifromForPerVertex; //material specular
GLuint kdUniformForPerVertex; //materail diffuse
GLuint kshiUniformForPerVertex; //mater

//declare 12 uniforms(3 -> matrix, 4 -> lights(with color), 4-> material(white), 1->keyDown)
GLuint modelMatrixUniformForPerFragment;
GLuint viewMatrixUniformForPerFragment;
GLuint perspectiveProjectionMatrixUniformForPerFragment;

GLuint lKeyPressUniformForPerFragment;
//Light properties
GLuint ldUniformForPerFragment; //light diffuse
GLuint lsUniformForPerFragment; //Light specular
GLuint laUniformForPerFragment; //light Ambient
GLuint lightPositionUniformForPerFragment; //light position
//material properties
GLuint kaUniformForPerFragment; //material ambient
GLuint ksUnifromForPerFragment; //material specular
GLuint kdUniformForPerFragment; //materail diffuse
GLuint kshiUniformForPerFragment; //mater


bool gbLighting;
bool gbPerVertexEnabled = TRUE;
bool gbPerFagmentEnabled = FALSE;

//mat4 -> present in vmath.h
//mat4 ->is a type for 4*4 matrix, ha transformation sathi used hoto
mat4 perspectiveProjectionMatrix; 

//For sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gNumVertices;
GLuint gNumElements;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	void display(void);
	void initialize(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass; //structure 12 member
	HWND hwnd;           //unsingned int ->>window handle
	MSG msg;             //struct 6 member
	TCHAR szAppName[] = TEXT("MyApp"); // ASCII (americal std code for info exchange) ,UNICODE 

	//code
	//initializaation of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX); //size of class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION); //2
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); //3
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);// 1-> lib fun
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass); //4

	if (fopen_s(&gpFile, "SSGLog.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Can't open the log file"), TEXT("Error:"), MB_OK);
		return -1;
	}

	//Get Width and Height
	OsWndHight = GetSystemMetrics(SM_CYSCREEN);
	OsWndWidth = GetSystemMetrics(SM_CXSCREEN);


	//create window 5
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Programmable: 3D Animation"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();
	ShowWindow(hwnd, iCmdShow);//6
	//UpdateWindow(hwnd);//7

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				display();
				update();
			}

		}

	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str1[] = TEXT("HELLO WORLD !!!");


	//fnction declaration
	void ToggleFullscreen(void);
	void resize(int, int);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:

		switch (wParam)
		{
		case 'q':
		case 'Q':
			DestroyWindow(hwnd);
			break;

		case VK_ESCAPE:
			ToggleFullscreen();
			break;

		case 'l':
		case 'L':
			if (gbLighting == TRUE)
			{
				gbLighting = FALSE;
			}
			else
			{
				gbLighting = TRUE;
			}
			break;

		case 'v':
		case 'V':
			gbPerVertexEnabled = TRUE;
			gbPerFagmentEnabled = FALSE;
			break;

		case 'f':
		case 'F':
			gbPerFagmentEnabled = TRUE;
			gbPerVertexEnabled = FALSE;
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;

	case WM_CREATE:
		//MessageBox(hwnd, TEXT("Welcome To Programmable Pipeline"), TEXT("My_Message"), MB_OK);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam)); //12
}

void initialize(void)
{
	fprintf(gpFile, "[DEBUG] Start initialization");
	fflush(gpFile);
	void resize(int, int);
	
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "choosePixelFormat failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, " SetPixelFormat failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	//step 3) Enableing the openGl extension by using below line
	// First Programmable programming line
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd); //If extension enable failed for PP , no need to procced further
	}

	//OpenGL related logs
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fflush(gpFile);

	fprintf(gpFile, "OpenGL render : %s \n", glGetString(GL_RENDERER));
	fflush(gpFile);

	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fflush(gpFile);

	//fprintf(gpFile, "GLSL (Graphics lib shading lang.) : %s \n", glGetString(GL_SHADING_LANG));

	//OpenGL enable extensions
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExt);
	for (int i = 0; i < numExt; i++)
	{
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
		fflush(gpFile);
	}

	// *** VERTEX SHADER ***
	// create shader	
	//--------------------- Vertex Shader For PerVertex -----------------------//
	fprintf(gpFile, "[DEBUG] start vertex shader(For PerVertex) \n");
	fflush(gpFile);

	gVertexShaderObjectForPerVertex = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertexShaderSourceCodeForPerVertex =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"\n" \
		"in vec3 vNormal;" \
		"\n" \
		"uniform mat4 u_modelMatrix;" \
		"\n" \
		"uniform mat4 u_viewMatrix;" \
		"\n" \
		"uniform mat4 u_projectionMatrix;" \
		"\n" \
		"uniform int u_lKeyPressed;" \
		"\n" \
		"uniform vec3 u_la;" \
		"\n" \
		"uniform vec3 u_ld;" \
		"\n" \
		"uniform vec3 u_ls;" \
		"\n" \
		"uniform vec4 u_lightPosition;" \
		"\n" \
		"uniform vec3 u_ka;" \
		"\n" \
		"uniform vec3 u_kd;" \
		"\n" \
		"uniform vec3 u_ks;" \
		"\n" \
		"uniform vec3 u_kshi;" \
		"uniform float c;" \
		"\n" \
		"out vec3 fongAdsLight;" \
		"\n" \
		"void main(void)" \
		"\n" \
		"{" \
		"\n" \
		"if(u_lKeyPressed == 1)" \
		"\n" \
			"{" \
		"\n" \
				// step1: cal eye cordiantes
				"vec4 eyeCordinate = u_viewMatrix * u_modelMatrix * vPosition;" \
		"\n" \
				// step2: cal transform normals
				"vec3 transformNormal = normalize(mat3(u_viewMatrix * u_modelMatrix) * vNormal);" \
		"\n" \
				// step3: cal source of light
				"vec3 lightDirection = normalize(vec3(u_lightPosition - eyeCordinate));" \
		"\n" \
				// step4: cal reflection vector
				"vec3 reflectionVector = reflect(-lightDirection,transformNormal);" \
		"\n" \
				// step5: cal view vector
				"vec3 normalizeViewVector = normalize(-eyeCordinate.xyz);" \
		"\n" \
				// step6: cal diffuse light by using light eq.

				//Light calculations
				// Ia =la * ka
				"vec3 lightAmbient = u_la * u_ka;" \
				// Id = ld * kd * (L.N)
				"vec3 lightDiffuse = u_ld * u_kd * max(dot(lightDirection, transformNormal), 0.0f);" \
				// Is = ls * ks * (R.V)^shin
		        "vec3 lightSpecular = u_ls * u_ks * max(dot(reflectionVector, normalizeViewVector), 0.0f);" \
				// total light intensity = Ia + Id + Is
				"fongAdsLight = lightAmbient + lightDiffuse + lightSpecular;" \
			"}" \

		"gl_Position = u_projectionMatrix * u_modelMatrix * u_viewMatrix * vPosition;" \
		
		"}";

	glShaderSource(gVertexShaderObjectForPerVertex, 1, (const GLchar**)&vertexShaderSourceCodeForPerVertex, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectForPerVertex);
	
	//Nothing to compile so no compile time error checking
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectForPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectForPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectForPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compelation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	fprintf(gpFile, "[DEBUG] end complie vertex shader(For PerVertex) \n");
	fflush(gpFile);
	//--------------------- Vertex Shader For PerFragment -----------------------//
	fprintf(gpFile, "[DEBUG] start vertex shader(Per Fragment) \n");
	fflush(gpFile);

	gVertexShaderObjectForPerFragment = glCreateShader(GL_VERTEX_SHADER);

	const GLchar* vertexShaderSourceCodeForPerFragment =
		
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"\n" \
		"in vec3 vNormal;" \
		"\n" \
		"uniform mat4 u_modelMatrix;" \
		"\n" \
		"uniform mat4 u_viewMatrix;" \
		"\n" \
		"uniform mat4 u_projectionMatrix;" \
		"\n" \
		"uniform int u_lKeyPressed;" \
		"\n" \
		"uniform vec4 u_lightPosition;" \
		"\n" \
		"out vec3 transformNormal;" \
		"\n" \
		"out vec3 lightDirection;" \
		"\n" \
		"out vec3 viewVector;" \
		"\n" \
		"void main(void)" \
		"\n" \
		"{" \
		"\n" \
		"if(u_lKeyPressed == 1)" \
		"\n" \
		"{" \
		"\n" \
		// step1: cal eye cordiantes
		"vec4 eyeCordinate = u_viewMatrix * u_modelMatrix * vPosition;" \
		"\n" \
		// step2: cal transform normals
		"transformNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal;" \
		"\n" \
		// step3: cal source of light
		"lightDirection = vec3(u_lightPosition - eyeCordinate);" \
		"\n" \
		// step5: cal view vector
		"viewVector = -eyeCordinate.xyz;" \
		"\n" \
		"}" \

		"gl_Position = u_projectionMatrix * u_modelMatrix * u_viewMatrix * vPosition;" \

		"}";

	glShaderSource(gVertexShaderObjectForPerFragment, 1, (const GLchar**)&vertexShaderSourceCodeForPerFragment, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectForPerFragment);

	//Nothing to compile so no compile time error checking
	glGetShaderiv(gVertexShaderObjectForPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectForPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectForPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compelation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	fprintf(gpFile, "[DEBUG] end complie vertex shader(For PerFragment) \n");
	fflush(gpFile);
	// ---------------------------------------------------------------------------//

	// *** FRAGMENT SHADER ***
	// create shader
	fprintf(gpFile, "[DEBUG] start fragment shader(For PerVertex) \n");
	fflush(gpFile);

	gFragmentShaderObjectForPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar* fragementShaderSourceCodeForPerVertex =
		"#version 430 core" \
		"\n" \
		"vec4 color;" \
		"\n" \
		"in vec3 fongAdsLight;" \
		"\n" \
		"uniform int u_lKeyPressed;" \
		"\n" \
		"out vec4 FragColor;" \
		"\n" \
		"void main(void)" \
		"\n" \
		"{" \
			"if(true)" \
			"{" \
				"color = vec4(fongAdsLight, 1.0f);" \
				"\n" \
			"}" \
			"else" \
			"{" \
				"color = vec4(1.0f, 1.0f, 1.0f, 1.0f);" \
				"\n" \
			"}" \
		
		"FragColor = color;" \
		"}";

	glShaderSource(gFragmentShaderObjectForPerVertex, 1, (const GLchar**)&fragementShaderSourceCodeForPerVertex, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObjectForPerVertex);

	//error checking
	glGetShaderiv(gFragmentShaderObjectForPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectForPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectForPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "1Fragement Shader Compelation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	fprintf(gpFile, "[DEBUG] end complie fragment shader(For PerVertex) \n");
	fflush(gpFile);
	//-------------------------------------------------------------------------------

	fprintf(gpFile, "[DEBUG] start fragment shader(For Perfragment) \n");
	fflush(gpFile);

	gFragmentShaderObjectForPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar* fragementShaderSourceCodeForPerFragment =
		"#version 430 core" \
		"\n" \
		"vec4 color;" \
		"\n" \
		"in vec3 transformNormal;" \
		"\n" \
		"in vec3 lightDirection;" \
		"\n" \
		"in vec3 viewVector;" \
		"\n" \
		"uniform int u_lKeyPressed;" \
		"\n" \
		"uniform vec3 u_la;" \
		"\n" \
		"uniform vec3 u_ld;" \
		"\n" \
		"uniform vec3 u_ls;" \
		"\n" \
		"uniform vec3 u_ka;" \
		"\n" \
		"uniform vec3 u_kd;" \
		"\n" \
		"uniform vec3 u_ks;" \
		"\n" \
		"uniform float u_kshi;" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(true)" \
		"{" \
		"\n" \
		"vec3 normalizeTransformNormal = normalize(transformNormal);" \
		"\n" \
		"vec3 normalizeLightDirection = normalize(lightDirection);" \
		"\n" \
		"vec3 normalizeViewVector = normalize(viewVector);" \
		"\n" \
		// cal reflection vector
		"vec3 reflectionVector = reflect(-normalizeLightDirection,normalizeTransformNormal);" \
		"\n" \

		//cal diffuse light by using light eq.
		// Ia =la * ka
		"vec3 lightAmbient = u_la * u_ka;" \
		"\n" \
		// Id = ld * kd * (L.N)
		"vec3 lightDiffuse = u_ld * u_kd * max(dot(normalizeLightDirection, normalizeTransformNormal), 0.0f);" \
		"\n" \
		// Is = ls * ks * (R.V)^shin
		"vec3 lightSpecular = u_ls * u_ks * pow(max(dot(reflectionVector, normalizeViewVector), 0.0f), u_kshi);" \
		"\n" \
		// total light intensity = Ia + Id + Is
		"vec3 fongAdsLight = lightAmbient + lightDiffuse + lightSpecular;" \
		"\n" \
		"color = vec4(fongAdsLight, 1.0f);" \
		"}" \
		"else" \
		"{" \
		"color = vec4(1.0f, 1.0f, 1.0f, 1.0f);" \
		"}" \

		"FragColor = color;" \

		"}";

	glShaderSource(gFragmentShaderObjectForPerFragment, 1, (const GLchar**)&fragementShaderSourceCodeForPerFragment, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObjectForPerFragment);

	//error checking
	glGetShaderiv(gFragmentShaderObjectForPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectForPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectForPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragement Shader Compelation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	fprintf(gpFile, "[DEBUG] end complie fragment shader(For Perfragment) \n");
	fflush(gpFile);
	//----------------------------------------------------------------------------//
	
	// ***SHADER PROGRAM **
	
	//----------------------------------------------------------------------------//

	//For PerVertex
	fprintf(gpFile, "[DEBUG] start fragment shader Object (For PerVertex) \n");
	fflush(gpFile);
	gShaderProgramObjectForPerVertex = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectForPerVertex, gVertexShaderObjectForPerVertex);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObjectForPerVertex, gFragmentShaderObjectForPerVertex);

	//pre-link binding of shader program object with vertex shader position
	//atribute
	glBindAttribLocation(gShaderProgramObjectForPerVertex, SSG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectForPerVertex, SSG_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObjectForPerVertex, SSG_ATTRIBUTE_NORMAL, "vNormal");

	//link shader
	glLinkProgram(gShaderProgramObjectForPerVertex);

	GLint iShaderProgramLinkStatus = 0;

	//get link error checking
	glGetProgramiv(gShaderProgramObjectForPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectForPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectForPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %d %s\n", iInfoLogLength, szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	fprintf(gpFile, "[DEBUG] end fragment shader Object (For PerVertex) \n");
	fflush(gpFile);

	//For PerFragemnt
	fprintf(gpFile, "[DEBUG] start fragment shader Object (For PerFragment) \n");
	fflush(gpFile);
	gShaderProgramObjectForPerFragment = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectForPerFragment, gVertexShaderObjectForPerFragment);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObjectForPerFragment, gFragmentShaderObjectForPerFragment);

	//pre-link binding of shader program object with vertex shader position
	//atribute
	glBindAttribLocation(gShaderProgramObjectForPerFragment, SSG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectForPerFragment, SSG_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObjectForPerFragment, SSG_ATTRIBUTE_NORMAL, "vNormal");

	//link shader
	glLinkProgram(gShaderProgramObjectForPerFragment);

	iShaderProgramLinkStatus = 0;

	//get link error checking
	glGetProgramiv(gShaderProgramObjectForPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectForPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectForPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	fprintf(gpFile, "[DEBUG] end fragment shader Object (For PerFragment) \n");
	fflush(gpFile);

	//------------------ Get mvp uniform location  -------------------------------//
	modelMatrixUniformForPerVertex  = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_modelMatrix");
	viewMatrixUniformForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_viewMatrix");
	perspectiveProjectionMatrixUniformForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_projectionMatrix");
	lKeyPressUniformForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_lKeyPressed");

	ldUniformForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_ld");
	lsUniformForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_ls");
	laUniformForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_la");
	lightPositionUniformForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_lightPosition");

	kaUniformForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_ka");
	kdUniformForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_kd");
	ksUnifromForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_ks");
	kshiUniformForPerVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_kshi");

	////For PerFrgament 
	modelMatrixUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_modelMatrix");
	viewMatrixUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_viewMatrix");
	perspectiveProjectionMatrixUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_projectionMatrix");
	lKeyPressUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_lKeyPressed");

	ldUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_ld");
	lsUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_ls");
	laUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_la");
	lightPositionUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_lightPosition");

	kaUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_ka");
	kdUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_kd");
	ksUnifromForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_ks");
	kshiUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_kshi");

	//-------------------------------------------------------------------------------//

	//*** vertices, color, shader attribs, vbo, vao initialization ***

	//Get sphere vertices and normal
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	// ***** For Square ***** //
	glGenVertexArrays(1, &vaoSphere);
	glBindVertexArray(vaoSphere);

	//For vertex
	glGenBuffers(1, &vboPositionSphere);
	glBindBuffer(GL_ARRAY_BUFFER, vboPositionSphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//For normals
	glGenBuffers(1, &vboNormalsSphere);
	glBindBuffer(GL_ARRAY_BUFFER, vboNormalsSphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vbo element
	glGenBuffers(1, &vboElementsSphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboElementsSphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//For 3D geomertry
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	gbLighting = false;

	//set orthographicMatrix to identitu matrix
	perspectiveProjectionMatrix  = mat4::identity();

	fprintf(gpFile, "[DEBUG] End initialization \n");
	fflush(gpFile);
	resize(WINWIDTH, WINHEIGHT);

}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
					(GLfloat)width / (GLfloat)height,
					0.1f,
					100.0f);
}

void display(void)
{

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//OpenGL drawing
	// **** For traingle ****//
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 projectionMatrix = mat4::identity();

	//translate by -10
	mat4 translateMatrix;
	translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	
	modelMatrix = translateMatrix;

	//multiplay the modelview and ortho matrix to get modelViewProjection matrix
	projectionMatrix = perspectiveProjectionMatrix;

	if (gbPerVertexEnabled)
	{
		//start using OpneGL program object
		glUseProgram(gShaderProgramObjectForPerVertex);
		glUniformMatrix4fv(modelMatrixUniformForPerVertex, 1, GL_FALSE, modelMatrix);//model space to world space 
		glUniformMatrix4fv(viewMatrixUniformForPerVertex, 1, GL_FALSE, viewMatrix); //for world to view space
		glUniformMatrix4fv(perspectiveProjectionMatrixUniformForPerVertex, 1, GL_FALSE, projectionMatrix); // for view to clip space

		if (gbLighting == true)
		{
			//Enable ligthing we have 4 uniform (1 -> keypressed, 3 -> lights)
			glUniform1i(lKeyPressUniformForPerVertex, 1);
			glUniform3f(laUniformForPerVertex, 0.0f, 0.0f, 0.0f);
			glUniform3f(lsUniformForPerVertex, 1.0f, 1.0f, 1.0f);
			glUniform3f(ldUniformForPerVertex, 1.0f, 1.0f, 1.0f); //light color
			GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f }; // w= 1 -> positional light , 0-> directional light
			glUniform4fv(lightPositionUniformForPerVertex, 1, (GLfloat*)lightPosition);


			glUniform3f(kaUniformForPerVertex, 0.0f, 0.0f, 0.0f);
			glUniform3f(ksUnifromForPerVertex, 1.0f, 1.0f, 1.0f);
			glUniform3f(kdUniformForPerVertex, 1.0f, 1.0f, 1.0f); // material diffusee
			glUniform1f(kshiUniformForPerVertex, 128.0f);

		}
		else
		{
			glUniform1i(lKeyPressUniformForPerVertex, 0);
		}


	}
	else if(gbPerFagmentEnabled)
	{
		//start using OpneGL program object
		glUseProgram(gShaderProgramObjectForPerFragment);
		glUniformMatrix4fv(modelMatrixUniformForPerFragment, 1, GL_FALSE, modelMatrix);//model space to world space 
		glUniformMatrix4fv(viewMatrixUniformForPerFragment, 1, GL_FALSE, viewMatrix); //for world to view space
		glUniformMatrix4fv(perspectiveProjectionMatrixUniformForPerFragment, 1, GL_FALSE, projectionMatrix); // for view to clip space

		if (gbLighting == true)
		{
			//Enable ligthing we have 4 uniform (1 -> keypressed, 3 -> lights)
			glUniform1i(lKeyPressUniformForPerFragment, 1);
			glUniform3f(laUniformForPerFragment, 0.0f, 0.0f, 0.0f);
			glUniform3f(lsUniformForPerFragment, 1.0f, 1.0f, 1.0f);
			glUniform3f(ldUniformForPerFragment, 1.0f, 1.0f, 1.0f); //light color
			GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f }; // w= 1 -> positional light , 0-> directional light
			glUniform4fv(lightPositionUniformForPerFragment, 1, (GLfloat*)lightPosition);


			glUniform3f(kaUniformForPerFragment, 0.0f, 0.0f, 0.0f);
			glUniform3f(ksUnifromForPerFragment, 1.0f, 1.0f, 1.0f);
			glUniform3f(kdUniformForPerFragment, 1.0f, 1.0f, 1.0f); // material diffusee
			glUniform1f(kshiUniformForPerFragment, 128.0f);

		}
		else
		{
			glUniform1i(lKeyPressUniformForPerFragment, 0);
		}

	}
	
	//bind vao
	glBindVertexArray(vaoSphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboElementsSphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//stop using OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);

}

void update(void)
{


}
void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		//set the window placementjust like before the fullscreen
		SetWindowPlacement(ghwnd, &wpPrev);

		//set the window position just like it was before the fullscreen
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// as we are back to normal window show the mouse cursor
		ShowCursor(true);

	}

	if (vboPositionSphere)
	{
		glDeleteBuffers(1, &vboPositionSphere);
		vboPositionSphere = 0;
	}

	if (vboNormalsSphere)
	{
		glDeleteBuffers(1, &vboNormalsSphere);
		vboNormalsSphere = 0;
	}

	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObjectForPerVertex, gVertexShaderObjectForPerVertex);
	//detach fragment shader from shader program object
	glDetachShader(gShaderProgramObjectForPerVertex, gFragmentShaderObjectForPerVertex);

	//delete vertex shader object
	glDeleteShader(gVertexShaderObjectForPerVertex);
	gVertexShaderObjectForPerVertex = 0;

	//delete fragment shader object
	glDeleteShader(gFragmentShaderObjectForPerVertex);
	gFragmentShaderObjectForPerVertex = 0;

	//For perFragment
	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObjectForPerFragment, gVertexShaderObjectForPerFragment);
	//detach fragment shader from shader program object
	glDetachShader(gShaderProgramObjectForPerFragment, gFragmentShaderObjectForPerFragment);

	//delete vertex shader object
	glDeleteShader(gVertexShaderObjectForPerFragment);
	gVertexShaderObjectForPerFragment = 0;

	//delete fragment shader object
	glDeleteShader(gFragmentShaderObjectForPerFragment);
	gFragmentShaderObjectForPerFragment = 0;


	//unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "Program is terminated successfully \n");
	fflush(gpFile);
	if (gpFile)
	{
		fclose(gpFile);
		free(gpFile);
	}

	
}

void ToggleFullscreen(void)
{
	//declare monitorinfo type variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//check fullscreen is there or not

	if (gbFullscreen == false)
	{
		//No
			// get cureent window style
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//check the current window style has WS_OVERLAPPEDWINDOW or not
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//if ------------------yes ->removed it
				//to do that
					//get the current window placaement and monitor info

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{

				SetWindowPos(ghwnd, HWND_TOP, 0, 0,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);

				// if both the above things are true then remove WS_OVERLAPPED from style
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				//set the possition of window accordingly so that it will occupy the whole screen
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		//as we are fullscreen hide the cursor conventially
		ShowCursor(false);
		gbFullscreen = true;
		//AS now we have done with fullscreen gbFullscreen = true
	}
	else {
		//yes

			//we are going to convert window to the normal we have to put back WS_OVERLAPPEDWINDOW
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		//set the window placementjust like before the fullscreen
		SetWindowPlacement(ghwnd, &wpPrev);
		
		//set the window position just like it was before the fullscreen
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// as we are back to normal window show the mouse cursor
		ShowCursor(true);
		// gbFullscreen= false
		gbFullscreen = false;
	}
}