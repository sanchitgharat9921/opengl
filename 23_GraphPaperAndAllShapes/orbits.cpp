/*
compilation:
cl.exe /c /EHsc /I C:\freeglut\include OGL.cpp
linking:
 link.exe OGL.obj /LIBPATH:C:\freeglut\lib /SUBSYSTEM:CONSOLE
*/

#include<windows.h>    
#include <stdio.h>
//step1) add header file 
#include "vmath.h" //graphics sathi laganri lib from readbook
#include <gl/glew.h> // add glew.h before GL.h
#include <gl/GL.h>
#include <math.h>


#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")
#define PI 3.14

using namespace vmath;

HDC ghdc = NULL;
HGLRC ghrc = NULL;

#define WINWIDTH 800
#define WINHEIGHT 600

//Fullscreen
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
HWND ghwnd = NULL;

bool gbActiveWindow = false;

FILE* gpFile = NULL;
int OsWndHight = 0;
int OsWndWidth = 0;

int AppWndHight = 400;
int AppWndWidth = 400;
TCHAR str[255];

//return val | calling convention | funname
// golbal function deeclarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

bool bDone = false;

GLuint gVertexShaderObjectHisto;
GLuint gFragmentShaderObjectHisto;
GLuint gShaderProgramObjectHisto;

//blobal varibable declarations
//below are the properties of vertex (imaginary point)
//It can have veelocity , gravity, damping, blur.
//below are most common properties of vertex
enum {
	SSG_ATTRIBUTE_POSITION = 0,
	SSG_ATTRIBUTE_COLOR,
	SSG_ATTRIBUTE_TEXCORD,
	SSG_ATTRIBUTE_NORMAL,
};

GLuint vao; // Vertex Array Object
GLuint vboPosition; // Vertex Buffer Object
GLuint vboColor;
GLuint mvpMAtrixUniform; //

GLuint vao_line;
GLuint vboPosition_line;
GLuint vboColor_line;

GLuint vao_square;
GLuint vboPosition_square;
GLuint vboColor_square;

GLuint vao_traingle;
GLuint vboPosition_traingle;
GLuint vboColor_traingle;

GLuint vao_circle;
GLuint vboPosition_circle;
GLuint vboColor_circle;
GLfloat circlePos[36000*3];
GLfloat circleColor[36000*3];

//mat4 -> present in vmath.h
//mat4 ->is a type for 4*4 matrix, ha transformation sathi used hoto
mat4 perspectiveProjectionMatrix; 


// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	void display(void);
	void initialize(void);

	//variable declarations
	WNDCLASSEX wndclass; //structure 12 member
	HWND hwnd;           //unsingned int ->>window handle
	MSG msg;             //struct 6 member
	TCHAR szAppName[] = TEXT("MyApp"); // ASCII (americal std code for info exchange) ,UNICODE 

	//code
	//initializaation of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX); //size of class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION); //2
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); //3
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);// 1-> lib fun
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass); //4

	if (fopen_s(&gpFile, "SSGLog.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Can't open the log file"), TEXT("Error:"), MB_OK);
		return -1;
	}

	//Get Width and Height
	OsWndHight = GetSystemMetrics(SM_CYSCREEN);
	OsWndWidth = GetSystemMetrics(SM_CXSCREEN);


	//create window 5
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGl doubble buffer"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();
	ShowWindow(hwnd, iCmdShow);//6
	//UpdateWindow(hwnd);//7

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{

				display();
			}

		}

	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str1[] = TEXT("HELLO WORLD !!!");


	//fnction declaration
	void ToggleFullscreen(void);
	void resize(int, int);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:

		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 'f':
		case 'F':
			ToggleFullscreen();
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;

	case WM_CREATE:
		MessageBox(hwnd, TEXT("Welcome Double Buffer App"), TEXT("My_Message"), MB_OK);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam)); //12
}

void initHistogramGraph(void)
{
	// *** VERTEX SHADER ***
	// create shader
	gVertexShaderObjectHisto = glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar* vertexShaderSourceCode =

		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;"\
		"out vec4 outColor;" \
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"\n" \
		"{" \
		"gl_Position = u_mvpMatrix * vPosition;" \
		"outColor = vColor;"\
		"}";

	glShaderSource(gVertexShaderObjectHisto, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectHisto);

	//Nothing to compile so no compile time error checking
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectHisto, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectHisto, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectHisto, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compelation Log: %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObjectHisto = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar* fragementShaderSourceCode =
		"#version 430" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec4 outColor;" \
		"void main(void)" \
		"{" \
		"FragColor = outColor;" \
		"}";

	glShaderSource(gFragmentShaderObjectHisto, 1, (const GLchar**)&fragementShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObjectHisto);

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	//error checking
	glGetShaderiv(gFragmentShaderObjectHisto, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectHisto, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectHisto, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragement Shader Compelation Log: %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	//get MVP uniform location

	// ***SHADER PROGRAM **
	//create
	gShaderProgramObjectHisto = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectHisto, gVertexShaderObjectHisto);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObjectHisto, gFragmentShaderObjectHisto);

	//pre-link binding of shader program object with vertex shader position
	//atribute
	glBindAttribLocation(gShaderProgramObjectHisto, SSG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectHisto, SSG_ATTRIBUTE_COLOR, "vColor");

	//link shader
	glLinkProgram(gShaderProgramObjectHisto);

	GLint iShaderProgramLinkStatus = 0;

	//get link error checking
	glGetProgramiv(gShaderProgramObjectHisto, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectHisto, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectHisto, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	//get mvp uniform location
	mvpMAtrixUniform = glGetUniformLocation(gShaderProgramObjectHisto, "u_mvpMatrix");

	//*** vertices, color, shader attribs, vbo, vao initialization ***
	const GLfloat traingleVertices[] =
	{
		0.0f,  0.5f, 0.0f,
	   -0.5f, -0.5f, 0.0f,
		0.5f, -0.5f, 0.0f
	};

	const GLfloat traingleColor[] =
	{
		1.0f,  1.0f, 1.0f,
		1.0f,  1.0f, 1.0f,
		1.0f,  1.0f, 1.0f,
	};

	const GLfloat linePoints[] = {
		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f
	};
	const GLfloat linePointsColor[] = {
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f
	};

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(traingleVertices), traingleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(traingleColor), traingleColor, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glGenVertexArrays(1, &vao_line);
	glBindVertexArray(vao_line);

	glGenBuffers(1, &vboPosition_line);
	glBindBuffer(GL_ARRAY_BUFFER, vboPosition_line);
	glBufferData(GL_ARRAY_BUFFER, sizeof(linePoints), linePoints, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vboColor_line);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor_line);
	glBufferData(GL_ARRAY_BUFFER, sizeof(linePointsColor), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

#if 0
	const GLfloat squarePos[] =
	{
		0.5f, 0.5f, 0.0f,
		-0.5f,0.5f, 0.0f,

		-0.5f,0.5f, 0.0f,
		-0.5f,-0.5f, 0.0f,

		-0.5f,-0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,

		0.5f, -0.5f, 0.0f,
		0.5f, 0.5f, 0.0f,
	};

	const GLfloat squareColor[] =
	{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
	};
#endif
	const GLfloat squarePos[] =
	{
		0.5f, 0.5f, 0.0f,
		-0.5f,0.5f, 0.0f,

		-0.5f,0.5f, 0.0f,
		-0.5f,-0.5f, 0.0f,

		-0.5f,-0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,

		0.5f, -0.5f, 0.0f,
		0.5f, 0.5f, 0.0f,
	};

	const GLfloat squareColor[] =
	{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

	};


	glGenVertexArrays(1, &vao_square);
	glBindVertexArray(vao_square);

	glGenBuffers(1, &vboPosition_square);
	glBindBuffer(GL_ARRAY_BUFFER, vboPosition_square);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squarePos), squarePos, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vboColor_square);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor_square);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareColor), squareColor, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	void getCirclePoints(float rad);
	//getCirclePoints(sqrt((0.5 * 0.5) + (0.5 * 0.5)));
	getCirclePoints(1.0f);

	glGenVertexArrays(1, &vao_circle);
	glBindVertexArray(vao_circle);

	glGenBuffers(1, &vboPosition_circle);
	glBindBuffer(GL_ARRAY_BUFFER, vboPosition_circle);
	glBufferData(GL_ARRAY_BUFFER, 36000 * 3 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vboColor_circle);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor_circle);
	glBufferData(GL_ARRAY_BUFFER, 36000 * 3 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void initialize(void)
{
	void resize(int, int);
	
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "choosePixelFormat failed \n");
		DestroyWindow(ghwnd);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, " SetPixelFormat failed \n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext failed \n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent failed \n");
		DestroyWindow(ghwnd);
	}

	//step 3) Enableing the openGl extension by using below line
	// First Programmable programming line
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd); //If extension enable failed for PP , no need to procced further
	}

	//OpenGL related logs
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));

	fprintf(gpFile, "OpenGL render : %s \n", glGetString(GL_RENDERER));

	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));

	//fprintf(gpFile, "GLSL (Graphics lib shading lang.) : %s \n", glGetString(GL_SHADING_LANG));

	//OpenGL enable extensions
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExt);
	for (int i = 0; i < numExt; i++)
	{
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	initHistogramGraph();

	//For 3D geomertry
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//set orthographicMatrix to identitu matrix
	perspectiveProjectionMatrix  = mat4::identity();

	resize(WINWIDTH, WINHEIGHT);

}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
					(GLfloat)width / (GLfloat)height,
					0.1f,
					100.0f);
}

void displayGraph(void)
{
	void drawGraphPaper(mat4 modeViewMatrix);
	void drawSquare(mat4 modeViewMatrix);
	void drawTraingle(mat4 modeViewMatrix);
	void drawCircle(mat4 modeViewMatrix);
	void drawXYAxis(mat4 modeViewMatrix);

	//start using OpneGL program object
	glUseProgram(gShaderProgramObjectHisto);

	//OpenGL drawing
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();

	//translate by -3
	mat4 translateMatrix;

	translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

	modelViewMatrix = translateMatrix;

	//multiplay the modelview and ortho matrix to get modelViewProjection matrix
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpMAtrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	drawGraphPaper(modelViewMatrix);
	//draw square
	drawSquare(modelViewMatrix);
	drawTraingle(modelViewMatrix);
	scaleMatrix = vmath::scale(0.707f, 0.707f, 0.707f);
	drawCircle(modelViewMatrix * scaleMatrix);
	scaleMatrix = mat4::identity();
	scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
	translateMatrix = vmath::translate(0.0f, -0.19f, 0.0f);
	drawCircle(modelViewMatrix * translateMatrix * scaleMatrix);

	//draw XY Axis
	drawXYAxis(modelViewMatrix);

	//stop using OpenGL program object
	glUseProgram(0);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	void displayGraph(void);

	displayGraph();

	SwapBuffers(ghdc);

}
void drawSquare(mat4 modelViewMatrix)
{
	mat4 modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpMAtrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);
#if 0
	glBindVertexArray(vao_square);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 2, 2);
	glDrawArrays(GL_LINES, 4, 2);
	glDrawArrays(GL_LINES, 6, 2);
	glBindVertexArray(0);
#endif
	glBindVertexArray(vao_square);
	for (int i = 0; i < 4; i++)
	{
		glDrawArrays(GL_LINE_LOOP, i *2, 2);
		//glDrawArrays(GL_LINE_LOOP, 2, 2);
		//glDrawArrays(GL_LINE_LOOP, 4, 2);
		//glDrawArrays(GL_LINE_LOOP, 6, 2);
	}

	glBindVertexArray(0);


}
void drawTraingle(mat4 modelViewMatrix)
{
	//draw traingle
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);
}
void getCirclePoints(float radious)
{
//eclipse
#if 1
	int x = 0;
	int y = 1;
	int z = 2;
	int i = 0;
	int j = 0;
	GLuint index = 0;
	int Angle = 0;
	for (Angle = 0; Angle < 362; Angle += 1)
	{
		//circlePos[i] = radious * cos(Angle * 3.14 / (180.0f));
		circlePos[i] = 0.57f * cos(Angle * 3.14 / (180.0f));
		i++;
		//circlePos[i] = radious * sin(Angle * 3.14 / (180.0f));
		circlePos[i] = 0.56f * sin(Angle * 3.14 / (180.0f));
		i++;
		circlePos[i] = 0.0f;
		i++;

		circleColor[j] = 1.0f;
		j++;
		circleColor[j] = 1.0f;
		j++;
		circleColor[j] = 1.0f;
		j++;
	}
	int last = i;
	last -= 1;
	//last -= 3;

	circlePos[last] = 0.0f;
	circlePos[last - 1] = 0.0f;
	circlePos[last - 2] = 0.5f;

	for (int index = 0; index < 360 * 3; index++)
	{
		fprintf(gpFile, "\n i = %d =>> pos[ ", index);
		fprintf(gpFile, "%f ", circlePos[index]);
		index++;
		fprintf(gpFile, "%f ", circlePos[index]);
		index++;
		fprintf(gpFile, "%f] \n", circlePos[index]);		
	}
#endif
#if 0
	int x = 0;
	int y = 1;
	int z = 2;
	int i = 0;
	int j = 0;
	GLuint index = 0;
	int Angle = 0;
	for (Angle = 0; Angle < 362; Angle += 1)
	{
		circlePos[i] = radious * cos(Angle * 3.14 / (180.0f));
		i++;
		circlePos[i] = radious * sin(Angle * 3.14 / (180.0f));
		i++;
		circlePos[i] = 0.0f;
		i++;

		circleColor[j] = 1.0f;
		j++;
		circleColor[j] = 1.0f;
		j++;
		circleColor[j] = 1.0f;
		j++;
	}
	int last = i;
	last -= 1;
	//last -= 3;

	circlePos[last] = 0.0f;
	circlePos[last - 1] = 0.0f;
	circlePos[last - 2] = 0.5f;

	for (int index = 0; index < 360 * 3; index++)
	{
		fprintf(gpFile, "\n i = %d =>> pos[ ", index);
		fprintf(gpFile, "%f ", circlePos[index]);
		index++;
		fprintf(gpFile, "%f ", circlePos[index]);
		index++;
		fprintf(gpFile, "%f] \n", circlePos[index]);		
	}
#endif

#if 0
	int numPoints = 314;
	int i = 0;
	int j = 0;
	GLuint index = 0;
	int Angle = 0;
	for (i = 0; i < numPoints * 3; i++)
	{
		Angle = (2 * 3.14 * i / numPoints);
		circlePos[i] = 0.5f * cos(Angle * 3.14 / (180.0f));
		i++;
		circlePos[i] = 0.5f * sin(Angle * 3.14 / (180.0f));
		i++;
		circlePos[i] = 0.0f;
		i++;

		circleColor[j] = 1.0f;
		j++;
		circleColor[j] = 1.0f;
		j++;
		circleColor[j] = 1.0f;
		j++;
	}

	for (int index = 0; index < 360 * 3; index++)
	{
		fprintf(gpFile, "\n i = %d =>> pos[ ", index);
		fprintf(gpFile, "%f ", [index]);
		index++;
		fprintf(gpFile, "%f ", circlePos[index]);
		index++;
		fprintf(gpFile, "%f] \n", circlePos[index]);
	}
#endif

}
void drawCircle(mat4 modelViewMatrix)
{
	mat4 modelViewProjectionMatrix = mat4::identity();
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpMAtrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao_circle);

	glBindBuffer(GL_ARRAY_BUFFER, vboPosition_circle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(circlePos), circlePos, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vboColor_circle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(circleColor), circleColor, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glBindVertexArray(vao_circle);
	
	glDrawArrays(GL_LINE_LOOP, 0, sizeof(circlePos));
	//glDrawArrays(GL_LINE_LOOP, 0, 314 * 3*4);
	glBindVertexArray(0);

}
void drawGraphPaper(mat4 modelViewMatrix)
{
	void verticalNLinesatLRSide(int nLines, mat4 modelViewMatrix);
	void HorizontalnLinesatUDSide(int nLines, mat4 modelViewMatrix);

	//draw GraphPaper lines
	//modelViewMatrix --> translate with -3 in z dir
	verticalNLinesatLRSide(20, modelViewMatrix);
	HorizontalnLinesatUDSide(20, modelViewMatrix);

}
void drawXYAxis(mat4 modelViewMatrix)
{
	//modelViewMatrix --> translate with -3 in z dir
	mat4 rotateMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	//For Y Axis
	const GLfloat linePointsColor[] = {
	1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f
	};

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpMAtrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//For color
	glBindBuffer(GL_ARRAY_BUFFER, vboColor_line);
	glBufferData(GL_ARRAY_BUFFER, sizeof(linePointsColor), linePointsColor, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(vao_line);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//For X Axis
	const GLfloat linePointsColor1[] = {
	0.0f, 1.0f, 0.0f,
	0.0f, 1.0f, 0.0f
	};

	rotateMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix  * rotateMatrix;
	glUniformMatrix4fv(mvpMAtrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//For color
	glBindBuffer(GL_ARRAY_BUFFER, vboColor_line);
	glBufferData(GL_ARRAY_BUFFER, sizeof(linePointsColor1), linePointsColor1, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(vao_line);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

}
void verticalNLinesatLRSide(int nLines, mat4 modelViewMatrix)
{

	float offset = 1.0f / nLines;
	int up = 0;
	//modelViewMatrix --> translate with -3 in z dir
	mat4 translateMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
	const GLfloat linePointsColor[] = {
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f
	};
	
	for (up = 0; up <= 20; up++)
	{
		translateMatrix = vmath::translate(up * offset ,0.0f, 0.0f);
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix * translateMatrix;
		glUniformMatrix4fv(mvpMAtrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//For color
		glBindBuffer(GL_ARRAY_BUFFER, vboColor_line);
		glBufferData(GL_ARRAY_BUFFER, sizeof(linePointsColor), linePointsColor, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(SSG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(SSG_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(vao_line);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

	}
	

	for (up = 0; up <= 20; up++)
	{
		translateMatrix = vmath::translate(-up * offset, 0.0f, 0.0f);
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix * translateMatrix;
		glUniformMatrix4fv(mvpMAtrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		//For Line color
		glBindBuffer(GL_ARRAY_BUFFER, vboColor_line);
		glBufferData(GL_ARRAY_BUFFER, sizeof(linePointsColor), linePointsColor, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(SSG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(SSG_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(vao_line);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

	}
}

void HorizontalnLinesatUDSide(int nLines, mat4 modelViewMatrix)
{
	float offset = 1.0f / nLines;
	int up = 0;
	//modelViewMatrix --> translate with -3 in z dir
	mat4 translateMatrix = mat4::identity();
	mat4 rotateMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();


	for (up = 0; up <= 20; up++)
	{
		translateMatrix = vmath::translate(0.0f, up * offset,0.0f);
		rotateMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(mvpMAtrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		glBindVertexArray(vao_line);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

	}

	for (up = 0; up <= 20; up++)
	{
		translateMatrix = vmath::translate(0.0f, -up * offset, 0.0f);
		rotateMatrix = vmath::rotate(90.0f, 0.0f, 0.0f, 1.0f);
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(mvpMAtrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		glBindVertexArray(vao_line);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

	}
}

void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		//set the window placementjust like before the fullscreen
		SetWindowPlacement(ghwnd, &wpPrev);

		//set the window position just like it was before the fullscreen
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// as we are back to normal window show the mouse cursor
		ShowCursor(true);

	}

	if (vboPosition)
	{
		glDeleteBuffers(1, &vboPosition);
		vboPosition = 0;
	}

	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObjectHisto, gVertexShaderObjectHisto);
	//detach fragment shader from shader program object
	glDetachShader(gShaderProgramObjectHisto, gFragmentShaderObjectHisto);

	//delete vertex shader object
	glDeleteShader(gVertexShaderObjectHisto);
	gVertexShaderObjectHisto = 0;

	//delete fragment shader object
	glDeleteShader(gFragmentShaderObjectHisto);
	gFragmentShaderObjectHisto = 0;

	//unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "Program is terminated successfully \n");
	if (gpFile)
	{
		fclose(gpFile);
		free(gpFile);
	}

	
}

void ToggleFullscreen(void)
{
	//declare monitorinfo type variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//check fullscreen is there or not

	if (gbFullscreen == false)
	{
		//No
			// get cureent window style
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//check the current window style has WS_OVERLAPPEDWINDOW or not
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//if ------------------yes ->removed it
				//to do that
					//get the current window placaement and monitor info

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{

				SetWindowPos(ghwnd, HWND_TOP, 0, 0,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);

				// if both the above things are true then remove WS_OVERLAPPED from style
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				//set the possition of window accordingly so that it will occupy the whole screen
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		//as we are fullscreen hide the cursor conventially
		ShowCursor(false);
		gbFullscreen = true;
		//AS now we have done with fullscreen gbFullscreen = true
	}
	else {
		//yes

			//we are going to convert window to the normal we have to put back WS_OVERLAPPEDWINDOW
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		//set the window placementjust like before the fullscreen
		SetWindowPlacement(ghwnd, &wpPrev);
		
		//set the window position just like it was before the fullscreen
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// as we are back to normal window show the mouse cursor
		ShowCursor(true);
		// gbFullscreen= false
		gbFullscreen = false;
	}
}