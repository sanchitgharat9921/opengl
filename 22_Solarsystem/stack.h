#include <cstdio> 
#include <stdexcept> 
#include "vmath.h" //graphics sathi laganri lib from readbook
using namespace vmath;
class Stack{
	private: 
		mat4 *pa; 
		int size; 
		int top_index; 

	public: 
		// CC 
		Stack(int s_size) : pa(new mat4[s_size]), size(s_size), top_index(-1) {
		
		}

		~Stack(){
			delete[] pa; 
		}

		void push(mat4 data){
			if(top_index+1 == size)
				throw std::overflow_error("Stack is full"); 
			pa[++top_index] = data; 
		}

		void pop(mat4* p_data){
			if(top_index == -1)
				throw std::underflow_error("cannot pop through empty stack"); 
			*p_data = pa[top_index--]; 
		}

		void top(mat4* p_data){
			if(top_index == -1)
				throw std::underflow_error("cannot top the empty stack"); 
			*p_data = pa[top_index]; 
		}

}; 
