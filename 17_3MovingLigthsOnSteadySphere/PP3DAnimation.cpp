/*
compilation:
cl.exe /c /EHsc /I C:\freeglut\include OGL.cpp
linking:
 link.exe OGL.obj /LIBPATH:C:\freeglut\lib /SUBSYSTEM:CONSOLE
*/

#include<windows.h>    
#include <stdio.h>
//step1) add header file 
#include "vmath.h" //graphics sathi laganri lib from readbook
#include <gl/glew.h> // add glew.h before GL.h
#include <gl/GL.h>
#include "Sphere.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib,"Sphere.lib")

using namespace vmath;

HDC ghdc = NULL;
HGLRC ghrc = NULL;

#define WINWIDTH 800
#define WINHEIGHT 600

//Fullscreen
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
HWND ghwnd = NULL;

bool gbActiveWindow = false;

FILE* gpFile = NULL;
int OsWndHight = 0;
int OsWndWidth = 0;

int AppWndHight = 400;
int AppWndWidth = 400;
TCHAR str[255];

//return val | calling convention | funname
// golbal function deeclarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

bool bDone = false;

//default for pervertex
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

//default for per fragment
GLuint gVertexShaderObjectPerFragment;
GLuint gFragmentShaderObjectPerFragment;
GLuint gShaderProgramObjectPerFragment;

//blobal varibable declarations
//below are the properties of vertex (imaginary point)
//It can have veelocity , gravity, damping, blur.
//below are most common properties of vertex
enum {
	SSG_ATTRIBUTE_POSITION = 0,
	SSG_ATTRIBUTE_COLOR,
	SSG_ATTRIBUTE_TEXCORD,
	SSG_ATTRIBUTE_NORMAL,
};

GLuint vaoSphere;
GLuint vboPositionSphere;
GLuint vboNormalsSphere;
GLuint vboElementsSphere;

//declare 12 uniforms(3 -> matrix, 4 -> lights(with color), 4-> material(white), 1->keyDown)
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint perspectiveProjectionMatrixUniform;
GLuint lKeyPressUniform;

GLuint modelMatrixUniformForPerFragment;
GLuint viewMatrixUniformForPerFragment;
GLuint perspectiveProjectionMatrixUniformForPerFragment;

//material properties
GLuint kaUniform; //material ambient
GLuint ksUnifrom; //material specular
GLuint kdUniform; //materail diffuse
GLuint kshiUniform; //mater

GLuint kaUniformForPerFragment; //material ambient
GLuint ksUnifromForPerFragment; //material specular
GLuint kdUniformForPerFragment; //materail diffuse
GLuint kshiUniformForPerFragment; //mater


bool gbLighting;

struct Light {
	//Light properties
	GLuint ldUniform[3]; //light diffuse
	GLuint lsUniform[3]; //Light specular
	GLuint laUniform[3]; //light Ambient
	GLuint lightPositionUniform[3]; //light position
};

struct Light light, lightPerFrag;

//mat4 -> present in vmath.h
//mat4 ->is a type for 4*4 matrix, ha transformation sathi used hoto
mat4 perspectiveProjectionMatrix; 

//For sphere
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gNumVertices;
GLuint gNumElements;
GLfloat angleRotation = 0.0f;

bool gbPerVertexEnabled = TRUE;
bool gbPerFagmentEnabled = FALSE;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	void display(void);
	void initialize(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass; //structure 12 member
	HWND hwnd;           //unsingned int ->>window handle
	MSG msg;             //struct 6 member
	TCHAR szAppName[] = TEXT("MyApp"); // ASCII (americal std code for info exchange) ,UNICODE 

	//code
	//initializaation of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX); //size of class
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION); //2
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); //3
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);// 1-> lib fun
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass); //4

	if (fopen_s(&gpFile, "SSGLog.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Can't open the log file"), TEXT("Error:"), MB_OK);
		return -1;
	}

	//Get Width and Height
	OsWndHight = GetSystemMetrics(SM_CYSCREEN);
	OsWndWidth = GetSystemMetrics(SM_CXSCREEN);


	//create window 5
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Programmable: 3D Animation"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();
	ShowWindow(hwnd, iCmdShow);//6
	//UpdateWindow(hwnd);//7

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				display();
				update();
			}

		}

	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str1[] = TEXT("HELLO WORLD !!!");


	//fnction declaration
	void ToggleFullscreen(void);
	void resize(int, int);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:

		switch (wParam)
		{
		case 'q':
		case 'Q':
			DestroyWindow(hwnd);
			break;

		case VK_ESCAPE:
			ToggleFullscreen();
			break;

		case 'l':
		case 'L':
			if (gbLighting == TRUE)
			{
				gbLighting = FALSE;
			}
			else
			{
				gbLighting = TRUE;
			}
			break;


		case 'v':
		case 'V':
			gbPerVertexEnabled = TRUE;
			gbPerFagmentEnabled = FALSE;
			break;

		case 'f':
		case 'F':
			gbPerFagmentEnabled = TRUE;
			gbPerVertexEnabled = FALSE;
			break;


		default:
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;

	case WM_CREATE:
		//MessageBox(hwnd, TEXT("Welcome To Programmable Pipeline"), TEXT("My_Message"), MB_OK);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam)); //12
}

void initialize(void)
{
	fprintf(gpFile, "[DEBUG] Start initialization");
	fflush(gpFile);
	void resize(int, int);
	
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "choosePixelFormat failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, " SetPixelFormat failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent failed \n");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	//step 3) Enableing the openGl extension by using below line
	// First Programmable programming line
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd); //If extension enable failed for PP , no need to procced further
	}

	//OpenGL related logs
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fflush(gpFile);

	fprintf(gpFile, "OpenGL render : %s \n", glGetString(GL_RENDERER));
	fflush(gpFile);

	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fflush(gpFile);

	//fprintf(gpFile, "GLSL (Graphics lib shading lang.) : %s \n", glGetString(GL_SHADING_LANG));

	//OpenGL enable extensions
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExt);
	for (int i = 0; i < numExt; i++)
	{
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
		fflush(gpFile);
	}

	// *** VERTEX SHADER ***
	// create shader
	fprintf(gpFile, "[DEBUG] start vertex shader \n");
	fflush(gpFile);
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		//provide source code to shader
	const GLchar* vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"\n" \
		"in vec3 vNormal;" \
		"\n" \
		"uniform mat4 u_modelMatrix;" \
		"\n" \
		"uniform mat4 u_viewMatrix;" \
		"\n" \
		"uniform mat4 u_projectionMatrix;" \
		"\n" \
		"uniform int u_lKeyPressed;" \
		"\n" \
		"uniform vec3 u_la[3];" \
		"\n" \
		"uniform vec3 u_ld[3];" \
		"\n" \
		"uniform vec3 u_ls[3];" \
		"\n" \
		"uniform vec4 u_lightPosition[3];" \
		"\n" \
		"uniform vec3 u_ka;" \
		"\n" \
		"uniform vec3 u_kd;" \
		"\n" \
		"uniform vec3 u_ks;" \
		"\n" \
		"uniform float u_kshi;" \
		"\n" \
		"out vec3 fongAdsLight;" \
		"\n" \
		"void main(void)" \
		"\n" \
		"{" \
		"\n" \
		"if(u_lKeyPressed == 1)" \
		"\n" \
			"{" \
				"\n" \
				// step1: cal eye cordiantes
				"vec4 eyeCordinate = u_viewMatrix * u_modelMatrix * vPosition;" \
				"\n" \
				// step2: cal transform normals
				"vec3 transformNormal = normalize(mat3(u_viewMatrix * u_modelMatrix) * vNormal);" \
				"\n" \
				// step5: cal view vector
				"vec3 viewVector = normalize(-eyeCordinate.xyz);" \
				"\n" \
				// step6: cal diffuse light by using light eq.
				"vec3 lightDirection[3];" \
				"vec3 reflectionVector[3];" \
				"vec3 lightAmbient[3];" \
				"vec3 lightDiffuse[3];" \
				"vec3 lightShiniess[3];" \
				"vec3 color = vec3(0.0f, 0.0f, 0.0f);" \
		        "vec3 color1 = vec3(0.0f, 0.0f, 0.0f);" \
				//Light calculations
				"for(int i =0; i<3; i++)" \
				"{" \
					"\n" \
					// step3: cal source of light
					"lightDirection[i] = normalize(vec3(u_lightPosition[i] - eyeCordinate));" \
					"\n" \
					// step4: cal reflection vector
					"reflectionVector[i] = reflect(-lightDirection[i],transformNormal);" \
					// Ia =la * ka
					"lightAmbient[i] = u_la[i] * u_ka;" \
					// Id = ld * kd * (L.N)
					"lightDiffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i], transformNormal), 0.0f);" \
					// Is = ls * ks * (R.V)
					"lightShiniess[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], viewVector), 0.0f), u_kshi);" \
					// total light intensity = Ia + Id + Is
					" color +=   lightAmbient[i] + lightDiffuse[i] + lightShiniess[i]; " \
				"}" \
			       "fongAdsLight = color;" \
			"}" \

		"gl_Position = u_projectionMatrix * u_modelMatrix * u_viewMatrix * vPosition;" \
		
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);
	
	fprintf(gpFile, "[DEBUG] start complie vertex shader \n");
	fflush(gpFile);

	//Nothing to compile so no compile time error checking
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compelation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	fprintf(gpFile, "[DEBUG] end complie vertex shader \n");
	fflush(gpFile);
	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar* fragementShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"vec4 color;" \
		"in vec3 fongAdsLight;" \
		"uniform int u_lKeyPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
			"if(u_lKeyPressed == 1)" \
			"{" \
				"color = vec4(fongAdsLight, 1.0f);" \
			"}" \
			"else" \
			"{" \
				"color = vec4(1.0f, 1.0f, 1.0f, 1.0f);" \
			"}" \
		
		"FragColor = color;" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar**)&fragementShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObject);
	
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	//error checking
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragement Shader Compelation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	//get MVP uniform location
		
	// ***SHADER PROGRAM **
	//create
	gShaderProgramObject = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//pre-link binding of shader program object with vertex shader position
	//atribute
	glBindAttribLocation(gShaderProgramObject, SSG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, SSG_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObject, SSG_ATTRIBUTE_NORMAL, "vNormal");

	//link shader
	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;

	//get link error checking
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	
    //--------------------------
	// *** Per Fagrment light ***
	// create shader
	fprintf(gpFile, "[DEBUG] start vertex shader For per fragment light \n");
	fflush(gpFile);
	gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);

	//provide source code to shader
	const GLchar* vertexShaderSourceCodePerFragment =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"\n" \
		"in vec3 vNormal;" \
		"\n" \
		"uniform mat4 u_modelMatrix;" \
		"\n" \
		"uniform mat4 u_viewMatrix;" \
		"\n" \
		"uniform mat4 u_projectionMatrix;" \
		"\n" \
		"uniform int u_lKeyPressed;" \
		"\n" \
		"uniform vec4 u_lightPosition[3];" \
		"\n" \
		"out vec3 transformNormal;" \
		"\n" \
		"out vec3 lightDirection[3];" \
		"\n" \
		"out vec3 viewVector;" \
		"\n" \
		"void main(void)" \
		"\n" \
		"{" \
			"\n" \
			"if(u_lKeyPressed == 1)" \
			"\n" \
			"{" \
				// cal eye cordiantes
				"vec4 eyeCordinate = u_viewMatrix * u_modelMatrix * vPosition;" \
				"\n" \
				"for(int i =0; i<3; i++)" \
				"{" \
					"\n" \
					// cal transform normals
					"transformNormal = (mat3(u_viewMatrix * u_modelMatrix) * vNormal);" \
					"\n" \
					// cal view vector
					"viewVector = (-eyeCordinate.xyz);" \
					"\n" \
					// cal source of light
					"lightDirection[i] = (vec3(u_lightPosition[i] - eyeCordinate));" \
					"\n" \
				"}" \
			"}" \
		"gl_Position = u_projectionMatrix * u_modelMatrix * u_viewMatrix * vPosition;" \

		"}";

	glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar**)&vertexShaderSourceCodePerFragment, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectPerFragment);

	fprintf(gpFile, "[DEBUG] start complie vertex shader \n");
	fflush(gpFile);

	//Nothing to compile so no compile time error checking
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compelation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	fprintf(gpFile, "[DEBUG] end complie vertex shader \n");
	fflush(gpFile);
	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar* fragementShaderSourceCodePerFragment =
		"#version 430 core" \
		"\n" \
		"vec4 color;" \
		"uniform int u_lKeyPressed;" \
		"in vec3 transformNormal;" \
		"\n" \
		"in vec3 lightDirection[3];" \
		"\n" \
		"in vec3 viewVector;" \
		"\n" \
		"uniform vec3 u_la[3];" \
		"\n" \
		"uniform vec3 u_ld[3];" \
		"\n" \
		"uniform vec3 u_ls[3];" \
		"\n" \
		"uniform vec3 u_ka;" \
		"\n" \
		"uniform vec3 u_kd;" \
		"\n" \
		"uniform vec3 u_ks;" \
		"\n" \
		"uniform float u_kshi;" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
			"if(u_lKeyPressed == 1)" \
			"{" \
				"\n" \
				"vec3 normalizeTransformNormal;" \
				"vec3 normalizeLightDirection[3]; " \
				"vec3 normalizeViewVector;" \
				"vec3 reflectionVector[3];" \
				"vec3 lightAmbient[3];" \
		        "vec3 lightDiffuse[3];" \
				"vec3 lightSpecular[3];" \
				"vec3 lightTemp;" \
				"vec3 fongAdsLight;" \
		        "for(int i=0; i<3; i++)" \
				 "{" \
						"normalizeTransformNormal = normalize(transformNormal);" \
						"\n" \
						"normalizeLightDirection[i] = normalize(lightDirection[i]);" \
						"\n" \
						"normalizeViewVector = normalize(viewVector);" \
						"\n" \
						// cal reflection vector
						"reflectionVector[i] = reflect(-normalizeLightDirection[i],normalizeTransformNormal);" \
						"\n" \
						//cal diffuse light by using light eq.
						// Ia =la * ka
						"lightAmbient[i] = u_la[i] * u_ka;" \
						"\n" \
						// Id = ld * kd * (L.N)
						" lightDiffuse[i] = u_ld[i] * u_kd * max(dot(normalizeLightDirection[i], normalizeTransformNormal), 0.0f);" \
						"\n" \
						// Is = ls * ks * (R.V)
						" lightSpecular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], normalizeViewVector), 0.0f) , u_kshi);" \
						"\n" \
						// total light intensity = Ia + Id + Is
						" lightTemp += lightAmbient[i] + lightDiffuse[i] + lightSpecular[i];" \
						"\n" \
				"}" \
				"fongAdsLight = lightTemp;" \
				"color = vec4(fongAdsLight, 1.0f);" \
			"}" \
			"else" \
			"{" \
				"color = vec4(1.0f, 0.0f, 0.0f, 1.0f);" \
			"}" \
			"FragColor = color;" \
		"}";

	glShaderSource(gFragmentShaderObjectPerFragment, 1, (const GLchar**)&fragementShaderSourceCodePerFragment, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObjectPerFragment);

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	//error checking
	glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragement Shader Compelation Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	//get MVP uniform location

	// ***SHADER PROGRAM **
	//create
	gShaderProgramObjectPerFragment = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	//pre-link binding of shader program object with vertex shader position
	//atribute
	glBindAttribLocation(gShaderProgramObjectPerFragment, SSG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectPerFragment, SSG_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObjectPerFragment, SSG_ATTRIBUTE_NORMAL, "vNormal");

	//link shader
	glLinkProgram(gShaderProgramObjectPerFragment);

	iShaderProgramLinkStatus = 0;

	//get link error checking
	glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//------------------ Get mvp uniform location  -------------------------------//
	modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	perspectiveProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	lKeyPressUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");

	light.ldUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_ld[0]");
	light.lsUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_ls[0]");
	light.laUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_la[0]");
	light.lightPositionUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[0]");

	light.ldUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_ld[1]");
	light.lsUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_ls[1]");
	light.laUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_la[1]");
	light.lightPositionUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[1]");

	light.ldUniform[2] = glGetUniformLocation(gShaderProgramObject, "u_ld[2]");
	light.lsUniform[2] = glGetUniformLocation(gShaderProgramObject, "u_ls[2]");
	light.laUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_la[2]");
	light.lightPositionUniform[2] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[2]");

	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUnifrom = glGetUniformLocation(gShaderProgramObject, "u_ks");
	kshiUniform = glGetUniformLocation(gShaderProgramObject, "u_kshi");

	//For perFragment
	modelMatrixUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_modelMatrix");
	viewMatrixUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_viewMatrix");
	perspectiveProjectionMatrixUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_projectionMatrix");
	lKeyPressUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lKeyPressed");

	lightPerFrag.ldUniform[0] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ld[0]");
	lightPerFrag.lsUniform[0] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ls[0]");
	lightPerFrag.laUniform[0] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_la[0]");
	lightPerFrag.lightPositionUniform[0] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lightPosition[0]");

	lightPerFrag.ldUniform[1] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ld[1]");
	lightPerFrag.lsUniform[1] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ls[1]");
	lightPerFrag.laUniform[1] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_la[1]");
	lightPerFrag.lightPositionUniform[1] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lightPosition[1]");

	lightPerFrag.ldUniform[2] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ld[2]");
	lightPerFrag.lsUniform[2] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ls[2]");
	lightPerFrag.laUniform[1] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_la[2]");
	lightPerFrag.lightPositionUniform[2] = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lightPosition[2]");

	kaUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ka");
	kdUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_kd");
	ksUnifromForPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ks");
	kshiUniformForPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_kshi");



	//-------------------------------------------------------------------------------//

	//*** vertices, color, shader attribs, vbo, vao initialization ***

	//Get sphere vertices and normal
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	// ***** For Square ***** //
	glGenVertexArrays(1, &vaoSphere);
	glBindVertexArray(vaoSphere);

	//For vertex
	glGenBuffers(1, &vboPositionSphere);
	glBindBuffer(GL_ARRAY_BUFFER, vboPositionSphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//For normals
	glGenBuffers(1, &vboNormalsSphere);
	glBindBuffer(GL_ARRAY_BUFFER, vboNormalsSphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(SSG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SSG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vbo element
	glGenBuffers(1, &vboElementsSphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboElementsSphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	//For 3D geomertry
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	gbLighting = false;

	//set orthographicMatrix to identitu matrix
	perspectiveProjectionMatrix  = mat4::identity();

	fprintf(gpFile, "[DEBUG] End initialization \n");
	fflush(gpFile);
	resize(WINWIDTH, WINHEIGHT);

}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
					(GLfloat)width / (GLfloat)height,
					0.1f,
					100.0f);
}

void display(void)
{
	GLfloat lightPositionR[4] = { 0 };
	GLfloat lightPositionG[4] = { 0 };
	GLfloat lightPositionB[4] = { 0 };
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	if (gbPerVertexEnabled)
	{
		//start using OpneGL program object
		glUseProgram(gShaderProgramObject);
		//Light
		if (gbLighting == true)
		{
			//Enable ligthing we have 4 uniform (1 -> keypressed, 3 -> lights)
			glUniform1i(lKeyPressUniform, 1);
			glUniform3f(light.laUniform[0], 0.0f, 0.0f, 0.0f);
			glUniform3f(light.lsUniform[0], 1.0f, 0.0f, 0.0f);
			glUniform3f(light.ldUniform[0], 1.0f, 0.0f, 0.0f); //light color
			lightPositionR[0] = 0.0f;
			lightPositionR[1] = (GLfloat)cos(angleRotation) * 1;
			lightPositionR[2] = (GLfloat)sin(angleRotation) * 1;
			lightPositionR[3] = 1.0f;
			glUniform4fv(light.lightPositionUniform[0], 1, (GLfloat*)lightPositionR);

			glUniform3f(light.laUniform[1], 0.0f, 0.0f, 0.0f);
			glUniform3f(light.lsUniform[1], 0.0f, 0.0f, 1.0f);
			glUniform3f(light.ldUniform[1], 0.0f, 0.0f, 1.0f); //light color
			lightPositionB[0] = { (GLfloat)cos(angleRotation) * 1 };
			lightPositionB[1] = { 0.0f };
			lightPositionB[2] = { (GLfloat)sin(angleRotation) * 1 };
			lightPositionB[3] = { 1.0f };
			glUniform4fv(light.lightPositionUniform[1], 1, (GLfloat*)lightPositionB);
			glUniform3f(kaUniform, 0.0f, 0.0f, 0.0f);
			glUniform3f(ksUnifrom, 1.0f, 1.0f, 1.0f);
			glUniform3f(kdUniform, 1.0f, 1.0f, 1.0f); // material diffusee
			glUniform1f(kshiUniform, 128.0f);

			glUniform3f(light.laUniform[2], 0.0f, 0.0f, 0.0f);
			glUniform3f(light.lsUniform[2], 0.0f, 1.0f, 0.0f);
			glUniform3f(light.ldUniform[2], 0.0f, 1.0f, 0.0f); //light color
			lightPositionG[0] = { (GLfloat)cos(angleRotation) * 1 };
			lightPositionG[1] = { (GLfloat)sin(angleRotation) * 1 };
			lightPositionG[2] = { 0.0f };
			lightPositionG[3] = { 1.0f };
			glUniform4fv(light.lightPositionUniform[2], 1, (GLfloat*)lightPositionG);
			glUniform3f(kaUniform, 0.0f, 0.0f, 0.0f);
			glUniform3f(ksUnifrom, 1.0f, 1.0f, 1.0f);
			glUniform3f(kdUniform, 1.0f, 1.0f, 1.0f); // material diffusee
			glUniform1f(kshiUniform, 128.0f);

		}
		else
		{
			glUniform1i(lKeyPressUniform, 0);
		}

	}
	else if (gbPerFagmentEnabled)
	{
		glUseProgram(gShaderProgramObjectPerFragment);

		//Light
		if (gbLighting == true)
		{
			//Enable ligthing we have 4 uniform (1 -> keypressed, 3 -> lights)
			glUniform1i(lKeyPressUniform, 1);
			glUniform3f(lightPerFrag.laUniform[0], 0.0f, 0.0f, 0.0f);
			glUniform3f(lightPerFrag.lsUniform[0], 1.0f, 0.0f, 0.0f);
			glUniform3f(lightPerFrag.ldUniform[0], 1.0f, 0.0f, 0.0f); //light color
			lightPositionR[0] = 0.0f;
			lightPositionR[1] = (GLfloat)cos(angleRotation) * 100;
			lightPositionR[2] = (GLfloat)sin(angleRotation) * 100;
			lightPositionR[3] = 1.0f;
			glUniform4fv(lightPerFrag.lightPositionUniform[0], 1, (GLfloat*)lightPositionR);

			glUniform3f(lightPerFrag.laUniform[1], 0.0f, 0.0f, 0.0f);
			glUniform3f(lightPerFrag.lsUniform[1], 0.0f, 0.0f, 1.0f);
			glUniform3f(lightPerFrag.ldUniform[1], 0.0f, 0.0f, 1.0f); //light color
			lightPositionB[0] = { (GLfloat)cos(angleRotation) * 100 };
			lightPositionB[1] = { 0.0f };
			lightPositionB[2] = { (GLfloat)sin(angleRotation) * 100 };
			lightPositionB[3] = { 1.0f };
			glUniform4fv(lightPerFrag.lightPositionUniform[1], 1, (GLfloat*)lightPositionB);
			glUniform3f(kaUniform, 0.0f, 0.0f, 0.0f);
			glUniform3f(ksUnifrom, 1.0f, 1.0f, 1.0f);
			glUniform3f(kdUniform, 1.0f, 1.0f, 1.0f); // material diffusee
			glUniform1f(kshiUniform, 128.0f);

			glUniform3f(lightPerFrag.laUniform[2], 0.0f, 0.0f, 0.0f);
			glUniform3f(lightPerFrag.lsUniform[2], 0.0f, 1.0f, 0.0f);
			glUniform3f(lightPerFrag.ldUniform[2], 0.0f, 1.0f, 0.0f); //light color
			lightPositionG[0] = { (GLfloat)cos(angleRotation) * 100 };
			lightPositionG[1] = { (GLfloat)sin(angleRotation) * 100 };
			lightPositionG[2] = { 0.0f };
			lightPositionG[3] = { 1.0f };
			glUniform4fv(lightPerFrag.lightPositionUniform[2], 1, (GLfloat*)lightPositionG);
			glUniform3f(kaUniformForPerFragment, 0.0f, 0.0f, 0.0f);
			glUniform3f(kshiUniformForPerFragment, 1.0f, 1.0f, 1.0f);
			glUniform3f(kdUniformForPerFragment, 1.0f, 1.0f, 1.0f); // material diffusee
			glUniform1f(kshiUniformForPerFragment, 128.0f);

		}
		else
		{
			glUniform1i(lKeyPressUniform, 0);
		}
	}
	

	//OpenGL drawing
	// **** For traingle ****//
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 projectionMatrix = mat4::identity();

	//translate by -10
	mat4 translateMatrix;
	translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

	modelMatrix = translateMatrix;
	
	//multiplay the modelview and ortho matrix to get modelViewProjection matrix
	projectionMatrix = perspectiveProjectionMatrix ;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);//model space to world space 
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix); //for world to view space
	glUniformMatrix4fv(perspectiveProjectionMatrixUniform, 1, GL_FALSE, projectionMatrix); // for view to clip space

	glUniformMatrix4fv(modelMatrixUniformForPerFragment, 1, GL_FALSE, modelMatrix);//model space to world space 
	glUniformMatrix4fv(viewMatrixUniformForPerFragment, 1, GL_FALSE, viewMatrix); //for world to view space
	glUniformMatrix4fv(perspectiveProjectionMatrixUniformForPerFragment, 1, GL_FALSE, projectionMatrix); // for view to clip space

	//bind vao
	glBindVertexArray(vaoSphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboElementsSphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);	
	glBindVertexArray(0);

	//stop using OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);

}

void update(void)
{
	if (angleRotation >= 360.0f)
	{
		angleRotation = 0.0f;
	}
	else
	{
		angleRotation += 0.0005f;
	}

}
void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		//set the window placementjust like before the fullscreen
		SetWindowPlacement(ghwnd, &wpPrev);

		//set the window position just like it was before the fullscreen
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// as we are back to normal window show the mouse cursor
		ShowCursor(true);

	}

	if (vboPositionSphere)
	{
		glDeleteBuffers(1, &vboPositionSphere);
		vboPositionSphere = 0;
	}


	if (vboNormalsSphere)
	{
		glDeleteBuffers(1, &vboNormalsSphere);
		vboNormalsSphere = 0;
	}


	//detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	//detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	//delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	//delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	//unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	fprintf(gpFile, "Program is terminated successfully \n");
	fflush(gpFile);
	if (gpFile)
	{
		fclose(gpFile);
		free(gpFile);
	}

	
}

void ToggleFullscreen(void)
{
	//declare monitorinfo type variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//check fullscreen is there or not

	if (gbFullscreen == false)
	{
		//No
			// get cureent window style
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//check the current window style has WS_OVERLAPPEDWINDOW or not
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//if ------------------yes ->removed it
				//to do that
					//get the current window placaement and monitor info

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{

				SetWindowPos(ghwnd, HWND_TOP, 0, 0,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);

				// if both the above things are true then remove WS_OVERLAPPED from style
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				//set the possition of window accordingly so that it will occupy the whole screen
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		//as we are fullscreen hide the cursor conventially
		ShowCursor(false);
		gbFullscreen = true;
		//AS now we have done with fullscreen gbFullscreen = true
	}
	else {
		//yes

			//we are going to convert window to the normal we have to put back WS_OVERLAPPEDWINDOW
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		//set the window placementjust like before the fullscreen
		SetWindowPlacement(ghwnd, &wpPrev);
		
		//set the window position just like it was before the fullscreen
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// as we are back to normal window show the mouse cursor
		ShowCursor(true);
		// gbFullscreen= false
		gbFullscreen = false;
	}
}